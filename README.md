Pasul1: instalează ngrok și npm ( node )
Pasul2: Navighează către folderul "Restaurant" și deschide un terminal
Pasul3: rulează: ngrok http 5001 și copiază URL-ul de redirecționare (ar trebui să fie sub forma „https://6448-2a02-2f09-330b-2700-6900-fd53-38f1-d36d.ngrok-free.app”)
Pasul4: deschide fișierul app.js și actualizează link-ul ngrok existent cu cel nou.
Pasul5: navighează către folderul "the-project" și deschide fișierele .env si src/components/socket.js
Pasul6: în fișierele .env si socket.js, actualizează "REACT_APP_API_URL" si socket = io(url) cu URL-ul din pasul 3
Pasul7: din folderul "the-project", deschide un terminal nou și rulează comanda: serve -s build și copiază URL-ul de la linia „On Your Network:”, apoi oprește serverul cu control+c
Pasul8: cu URL-ul rețelei locale copiat, întoarce-te la Pasul4 și actualizează URL-urile existente care ar trebui să fie sub forma („http://192.168.1.168:3000”)
Pasul9: din folderul "Restaurant", deschide un terminal nou și rulează comanda: "node app.js"
Pasul10: din terminalul de la Pasul7, rulează rpm run build, odată finalizat, rulează comanda serve -s build
Pasul 11: Pentru portalul de client, accesează URL-ul rețelei locale de la pasul 7  sub forma: („http://192.168.1.168:3000/welcome/1”) unde ( 1 ) este numărul mesei
Pasul 12: Pentru portalul de chelner, accesează URL-ul rețelei locale de la pasul 7  sub forma: („http://192.168.1.168:3000/waiter”) și introdu: username: waiter; password: waiter
Pasul 13:  Pentru portalul de kitchen, accesează URL-ul rețelei locale de la pasul 7  sub forma: („http://192.168.1.168:3000/kitchen”) și introdu: username: kitchen; password: kitchen
Pasul 14:  Pentru portalul de admin, accesează URL-ul rețelei locale de la pasul 7  sub forma: („http://192.168.1.168:3000/admin”) și introdu: username: admin; password: admin