
export const generateInitialUniqueId = () => {

    let uniqueIdentifier = localStorage.getItem('uniqueIdentifier');
    console.log("Retrieved from localStorage:", uniqueIdentifier);
    if (!uniqueIdentifier || uniqueIdentifier === 'null') {
        uniqueIdentifier = `$${Math.random().toString(36).substring(7)}`;
        console.log("Generated new uniqueIdentifier:", uniqueIdentifier);
        localStorage.setItem('uniqueIdentifier', uniqueIdentifier);
    }
    return uniqueIdentifier;
}




export const updateUniqueIdWithNickname = (nickname, currentUniqueId) => {
    if (!currentUniqueId || currentUniqueId.indexOf('@') !== -1) {
        throw new Error('Invalid current unique ID.');
    }
    return `${nickname}@${currentUniqueId}`;
};

export const generateGuestUniqueId = (currentUniqueId) => {
    if (!currentUniqueId || currentUniqueId.indexOf('@') !== -1) {
        throw new Error('Invalid current unique ID.');
    }
    const guestPrefix = `Guest${Math.floor(Math.random() * 1000)}`;
    return `${guestPrefix}@${currentUniqueId}`;
};
