import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ParentComponent from './components/ParentComponent';
import { UniqueIdProvider } from './components/UniqueIdContext';
import WebSocketProvider from './components/WebSocketContext';
import { AuthProvider } from './components/AuthContext';

function App() {
  return (
    <AuthProvider>
      <Router>
        <WebSocketProvider>
          <UniqueIdProvider>
            <ParentComponent />
          </UniqueIdProvider>
        </WebSocketProvider>
      </Router>
    </AuthProvider>
  );
}

export default App;
