import { PieChart, Pie, Tooltip, Legend, Cell } from 'recharts';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#FF44AA', '#D32FF9', '#2FF3E0'];

const MostUsedTablesChart = ({ data }) => (
    <PieChart width={1000} height={400}>
        <Pie
            dataKey="count"
            isAnimationActive={true}
            data={data}
            cx="50%"
            cy="50%"
            outerRadius={80}
            label
        >
            {
                data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
            }
        </Pie>
        <Tooltip />
        <Legend />
    </PieChart>
);

export default MostUsedTablesChart;
