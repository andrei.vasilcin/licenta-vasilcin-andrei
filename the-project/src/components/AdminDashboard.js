import React, { useState, useEffect } from 'react';
import CRUDModal from './CRUDModal';
import AdminSidebar from './AdminSidebar';
import entityConfigs from './entityConfigs';
import TopSellingItemsChart from './TopSellingItemsChart';
import OrdersOverTimeChart from './OrdersOverTimeChart';
import MostUsedTablesChart from './MostUsedTablesChart';
import MostUsedIngredientsChart from './MostUsedIngredientsChart';
import axios from 'axios';
import './AdminDashboard.css';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

const API_URL = process.env.REACT_APP_API_URL;
const AdminDashboard = () => {


    const [activeEntity, setActiveEntity] = useState(null);
    const [action, setAction] = useState(null);
    const [data, setData] = useState({});
    const [validationErrors, setValidationErrors] = useState({});
    const [searchQuery, setSearchQuery] = useState('');
    const [filteredEntities, setFilteredEntities] = useState({});
    const [categories, setCategories] = useState([]);
    const [roles, setRoles] = useState([]);
    const [menuItems, setMenuItems] = useState([]);
    const [ingredients, setIngredients] = useState([]);
    const [showStats, setShowStats] = useState(false);
    const [animateEntities, setAnimateEntities] = useState(false)

    const [topSellingItems, setTopSellingItems] = useState([]);
    const [ordersOverTime, setOrdersOverTime] = useState([]);
    const [mostUsedTables, setMostUsedTables] = useState([]);
    const [mostUsedIngredients, setMostUsedIngredients] = useState([]);


    const useFormData = activeEntity === 'MenuItems';


    const [entities, setEntities] = useState({
        Categories: [],
        MenuItems: [],
        Users: [],
        Roles: [],
        Ingredients: [],
        MenuItemIngredients: [],
    });

    useEffect(() => {
        ingredients.forEach((ingredient) => {
            const expirationStatus = checkExpirationStatus(ingredient.expiry_date);

            if (expirationStatus === 'aboutToExpire') {
                toast.warn(`Ingredient ${ingredient.name} is about to expire!`, {
                    autoClose: false
                });
            } else if (expirationStatus === 'expired') {
                toast.error(`Ingredient ${ingredient.name} has expired!`, {
                    autoClose: false
                });
            }
        });
    }, [ingredients]);

    useEffect(() => {
        // fetch categories from the database
        axios.get(`${API_URL}/categories/`).then(response => setCategories(response.data));
        // fetch roles from the database
        axios.get(`${API_URL}/roles`).then((response) => setRoles(response.data));
        // fetch menu items from the database
        axios.get(`${API_URL}/menu-items`).then((response) => setMenuItems(response.data));
        // fetch ingredients from the database
        axios.get(`${API_URL}/ingredients`).then((response) => setIngredients(response.data));
        // fetch top-selling menu items
        axios.get(`${API_URL}/menu-items/stats/order-statistics`).then((response) => {
            const topSellingItems = response.data.map(item => ({
                name: item.name,
                orders: item.orders,
            }));
            setTopSellingItems(topSellingItems);
            // fetch orders over time
            axios.get(`${API_URL}/orders/stats/orders-over-time`)
                .then((response) => {
                    setOrdersOverTime(response.data);
                })
                .catch((error) => {
                    console.error('Error fetching orders over time:', error);
                });
        });
        // fetch most used tables
        axios.get(`${API_URL}/orders/stats/most-used-tables`).then((response) => {
            setMostUsedTables(response.data);
        });

        // fetch the most used ingredients
        axios.get(`${API_URL}/orders/stats/most-used-ingredients`).then((response) => {
            const mostUsedIngredientsData = response.data.map(ingredient => ({
                name: ingredient.ingredientName,
                count: ingredient.usageCount,
            }));
            setMostUsedIngredients(mostUsedIngredientsData);
        }).catch((error) => {
            console.error('Error fetching most used ingredients:', error);
        });

    }, []);

    // mapping categories to dropdown options
    const categoryOptions = categories.map(category => ({
        id: category.id,
        name: category.name,
    }));
    const handleEntitySelection = () => {
        setAnimateEntities(true);
        setTimeout(() => setAnimateEntities(false), 500);
    };
    const showStatistics = () => {
        setShowStats(true);
    };

    const hideStatistics = () => {
        setShowStats(false);
    };

    const transformedData = mostUsedTables.map(item => ({
        name: `Table ${item.tableNumber}`,
        count: item.count
    }));

    const handleModalClose = () => {
        setAction(null);
        setData({});
    };
    const handleDataChange = (updatedData) => {
        setData(updatedData);
    };
    // function to determine if an ingredient is about to expire or has expired
    const checkExpirationStatus = (expirationDate) => {
        const expiration = new Date(expirationDate);
        const now = new Date();
        const differenceInDays = (expiration - now) / (1000 * 60 * 60 * 24);

        if (differenceInDays <= 0) {
            return 'expired'; // expired
        } else if (differenceInDays <= 7) {
            return 'aboutToExpire'; // about to expire within a week
        }

        return 'valid'; // neither about to expire nor expired
    };
    const handleModalSubmit = async (data) => {

        if (activeEntity && action) {
            let method = 'post';
            let endpoint = entityConfigs[activeEntity].endpoints[action.toLowerCase()];
            // retrieve the existing image if it's an update and no new image is provided
            let existingImage = null;
            if (action.toLowerCase() === 'update' && !data.image) {
                const menuItem = entities.MenuItems.find(item => item.id === data.id);
                existingImage = menuItem ? menuItem.image_url : null;
            }

            // create FormData or a regular object based on the condition
            const requestDataObject = useFormData ? new FormData() : {};
            Object.keys(data).forEach((key) => {
                if (key === 'image' && existingImage && useFormData) {
                    requestDataObject.append('existingImageUrl', existingImage);
                } else {
                    if (useFormData) {
                        requestDataObject.append(key, data[key]);
                    } else {
                        requestDataObject[key] = data[key];
                    }
                }
            });

            let requestData = requestDataObject;
            const headers = {};
            if (!useFormData) {
                requestData = JSON.stringify(requestDataObject);
                headers['Content-Type'] = 'application/json';
            }

            if (action === 'Update') {
                method = 'put';
                endpoint = `${endpoint}/${data.id}`;
            }
            if (action === 'Delete') {
                method = 'delete';
                endpoint = `${endpoint}/${data.id}`;
            }
            console.log("data2?:", existingImage);
            try {
                // validate that all required fields are filled in
                const errors = {};
                entityConfigs[activeEntity].attributes.forEach((attribute) => {
                    if (!data[attribute.key]) {
                        errors[attribute.key] = `${attribute.label} is required.`;
                    }
                });

                // if there are any validation errors, update the state and return
                if (Object.keys(errors).length > 0) {
                    setValidationErrors(errors);
                    return;
                }

                const response = await axios[method](endpoint, requestData, { headers });
                fetchEntityData(activeEntity); // refresh the entity list after successful operation
            } catch (error) {
                console.error('Error performing CRUD operation:', error);
            }

            handleModalClose();
        }
    };



    const handleUpdateClick = (item) => {
        setData(item);
        setAction('Update');
    };

    const handleDelete = async (itemId) => {
        if (window.confirm("Are you sure you want to delete this item?")) {
            const endpoint = `${entityConfigs[activeEntity].endpoints.delete}/${itemId}`;
            try {
                await axios.delete(endpoint);
                fetchEntityData(activeEntity);
            } catch (error) {
                console.error('Error deleting item:', error);
            }
        }
    };


    const fetchEntityData = async (entityType) => {
        try {
            const response = await axios.get(entityConfigs[entityType].endpoints.read);
            setEntities(prevEntities => ({
                ...prevEntities,
                [entityType]: response.data
            }));
        } catch (error) {
            console.error(`Error fetching ${entityType}:`, error);
        }
    };
    useEffect(() => {
        Object.keys(entityConfigs).forEach(fetchEntityData);
    }, []);

    const handleSearch = (query) => {
        setSearchQuery(query);
        if (query) {
            const filtered = entities[activeEntity].filter(item =>
                Object.values(item).some(value =>
                    String(value).toLowerCase().includes(query.toLowerCase())
                )
            );
            setFilteredEntities({ [activeEntity]: filtered });
        } else {
            setFilteredEntities({});
        }
    };
    const clearError = (attributeKey) => {
        const newErrors = { ...validationErrors };
        delete newErrors[attributeKey];
        setValidationErrors(newErrors);
    };


    const EntityDisplay = ({ entityType, onUpdateClick, onDeleteClick }) => {
        const entityData = filteredEntities[entityType] || entities[entityType] || [];

        const getRoleName = (roleId) => {
            const role = roles.find((role) => role.id === roleId);
            return role ? role.name : 'Unknown Role';
        };

        const getMenuItem = (menuItemId) => {
            const menuItem = menuItems.find((menuItem) => menuItem.id === menuItemId);
            return menuItem ? menuItem.name : 'Unknown Menu Item Name';
        };

        const getCategoryName = (category_id) => {
            const category = categories.find((category) => category.id === category_id);
            return category ? category.name : 'Unknown Category';
        };

        const getIngredientName = (ingredient_id) => {
            const ingredient = ingredients.find((ingredient) => ingredient.id === ingredient_id);
            return ingredient ? ingredient.name : 'Unknown Ingredient';
        };

        return (
            <div className="entity-display">
                {entityData.map((item, index) => (
                    <div className="entity-display-item" key={index}>
                        {Object.entries(item).map(([key, value]) => (
                            <p key={key}>
                                <strong>{key}:</strong> {key === 'role' && entityType === 'Users' ? getRoleName(value) : key === 'category_id' && entityType === 'MenuItems' ? getCategoryName(value) : key === 'menu_item_id' && entityType === 'MenuItemIngredients' ? getMenuItem(value) : key === 'ingredient_id' && entityType === 'MenuItemIngredients' ? getIngredientName(value) : value}
                            </p>
                        ))}
                        <button className="admin-content-button" onClick={() => handleUpdateClick(item)}>Update</button>
                        <button className="admin-content-button" onClick={() => handleDelete(item.id)}>Delete</button>
                    </div>
                ))}
            </div>
        );
    };





    return (
        <div className="admin-dashboard">
            <AdminSidebar setActiveEntity={setActiveEntity} showStatistics={showStatistics} onEntitySelected={handleEntitySelection} />

            <div className={`admin-content ${animateEntities ? 'animate-entities' : ''}`}>
                {showStats ? (
                    <>
                        <h2>Statistics and Reports</h2>
                        <div className="chart-container">
                            <h3 className="chart-header">Top Selling Menu Items</h3>
                            <TopSellingItemsChart data={topSellingItems} />
                        </div>
                        <div className="chart-container">
                            <h3 className="chart-header">Orders Over Time</h3>
                            <OrdersOverTimeChart data={ordersOverTime} />
                        </div>
                        <div className="chart-container">
                            <h3 className="chart-header">Most Used Tables</h3>
                            <MostUsedTablesChart data={transformedData} />
                        </div>
                        <div className="chart-container">
                            <h3 className="chart-header">Most Used Ingredients</h3>
                            <MostUsedIngredientsChart data={mostUsedIngredients} />
                        </div>
                        <button className="hide-stats-button" onClick={hideStatistics}>Hide Statistics</button>
                    </>
                ) : (
                    <>
                        {activeEntity && (
                            <>
                                <h2>Manage {activeEntity}</h2>
                                <input
                                    type="text"
                                    className="search-bar"
                                    placeholder={`Search in ${activeEntity}`}
                                    value={searchQuery}
                                    onChange={(e) => handleSearch(e.target.value)}
                                />
                                <button onClick={() => setAction('Create')}>Create</button>

                                {action && (
                                    <CRUDModal
                                        entityType={activeEntity}
                                        action={action}
                                        data={data}
                                        onSubmit={handleModalSubmit}
                                        onClose={handleModalClose}
                                        entityConfig={entityConfigs[activeEntity]}
                                        setData={handleDataChange}
                                        errors={validationErrors}
                                        clearError={clearError}
                                        categoryOptions={categoryOptions}
                                        roles={roles}
                                        menuItems={menuItems}
                                        ingredients={ingredients}
                                    />
                                )}
                            </>
                        )}

                        {activeEntity && <EntityDisplay entityType={activeEntity} />}
                    </>
                )}
            </div>
            <ToastContainer />
        </div>
    );
};
export default AdminDashboard;
