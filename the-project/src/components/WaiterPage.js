import React, { useState, useEffect } from 'react';
import axios from 'axios';
import socket from './socket';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './WaiterPage.css';


const API_URL = process.env.REACT_APP_API_URL;


const WaiterPage = () => {
    const [tables, setTables] = useState([]);
    const [animateTable, setAnimateTable] = useState(null);
    const [pendingTableNotifications, setPendingTableNotifications] = useState([]);
    const [toastShownForTableStatus, setToastShownForTableStatus] = useState({});


    useEffect(() => {




        console.log("Connecting socket");
        socket.on('welcome', (message) => {
            console.log('Received:', message);
        });

        socket.on('ordersCreated', (newOrders) => {

            if (window.location.pathname !== '/waiter') {
                return;
            }

            handleOrderCreated(newOrders);
        });
        socket.on('orderUpdated', handleOrderUpdated);
        socket.on('orderDeleted', handleOrderDeleted);

        socket.on('helpRequested', (helpRequest) => {
            const { tableNumber, uniqueIdentifier } = helpRequest;



            toast.info(`Help requested at table ${tableNumber}`, {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: false,
                hideProgressBar: true,
                closeOnClick: true,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored",
            });

        });

        // fetch tables and orders when the component mounts
        fetchTables();

        // clean up socket connection when the component unmounts
        return () => {

            socket.off('ordersCreated', handleOrderCreated);
            socket.off('orderUpdated', handleOrderUpdated);
            socket.off('ordersDeleted', handleOrderDeleted);
            socket.off('helpRequested');

        };



    }, []);

    const fetchTables = async () => {
        try {
            const tablesResponse = await axios.get(`${API_URL}/tables`);
            const promises = tablesResponse.data.map(async (table) => {
                try {
                    const ordersResponse = await axios.get(`${API_URL}/orders/confirmed/${table.table_number}`);
                    const orders = ordersResponse.data.filter(order => order.status !== 'payed');
                    const groupedOrders = groupOrdersByCustomer(orders);

                    return {
                        ...table,
                        expanded: false,
                        groupedOrders: groupedOrders,
                        totalAmount: calculateTotalAmount(orders),
                    };
                } catch (error) {
                    console.error('Error fetching orders:', error);
                    return {
                        ...table,
                        expanded: false,
                        groupedOrders: {},
                        totalAmount: 0,
                    };
                }
            });

            const tablesWithOrders = await Promise.all(promises);
            setTables(tablesWithOrders);
        } catch (error) {
            console.error('Error fetching tables:', error);
        }
    };

    const handleConfirmGroup = async (customerIdentifier) => {
        try {
            const confirmPromises = tables.flatMap((table) =>
                table.groupedOrders[customerIdentifier]?.filter(order => order.status === 'pending').map((order) =>
                    axios.put(`${API_URL}/orders/${order.id}`, { status: 'confirmed' })
                )
            );

            await Promise.all(confirmPromises);
            await fetchTables();
        } catch (error) {
            console.error('Error confirming group orders:', error);
        }
    };



    const handleAllowModifications = async (orderId) => {
        try {
            let orderToModify;


            let parentTable;
            tables.forEach(table => {
                Object.keys(table.groupedOrders).forEach(customerIdentifier => {
                    const orderIndex = table.groupedOrders[customerIdentifier].findIndex(order => order.id === orderId);
                    if (orderIndex !== -1) {
                        orderToModify = table.groupedOrders[customerIdentifier][orderIndex];
                        parentTable = table;
                    }
                });
            });
            if (!orderToModify) {
                console.error('Order not found');
                return;
            }

            // create a new temporary order with the same data as the modified order
            const newTemporaryOrder = {

                table_number: orderToModify.table_number,
                customer_identifier: orderToModify.customer_identifier,
                total_price: orderToModify.total_price,
                status: 'temporary',
                order_time: orderToModify.order_time,
                menu_item_id: orderToModify.menu_item_id,
                quantity: orderToModify.quantity,
                remark: orderToModify.remark,


            };

            // add the new temporary order to the temporary orders table
            await axios.post(`${API_URL}/temporary-orders/waiter`, newTemporaryOrder);

            // remove the original order
            await axios.delete(`${API_URL}/orders/${orderId}`);

            await fetchTables();
        } catch (error) {
            console.error('Error allowing modifications:', error);
        }
    };



    const groupOrdersByCustomer = (orders) => {
        return orders.reduce((grouped, order) => {
            const { customer_identifier } = order;
            if (!grouped[customer_identifier]) {
                grouped[customer_identifier] = [];
            }
            grouped[customer_identifier].push(order);
            return grouped;
        }, {});
    };

    const calculateTotalAmount = (orders) => {
        return parseFloat(orders.reduce((total, order) => total + parseFloat(order.total_price * order.quantity), 0)).toFixed(2);
    };
    // function to toggle order display
    const toggleOrders = (index) => {
        if (animateTable === null) {

            setAnimateTable(index + 1);
            setTimeout(() => {
                setTables((prevTables) => {
                    const updatedTables = prevTables.map((table, i) => ({
                        ...table,
                        expanded: i === index ? !table.expanded : false,
                    }));
                    return updatedTables;
                });
            }, 200);
        } else {
            // reset animation state and collapse the table
            setAnimateTable(null);
            setTables((prevTables) => {
                const updatedTables = prevTables.map((table, i) => ({
                    ...table,
                    expanded: false,
                }));
                return updatedTables;
            });
        }
    };

    const handleOrderCreated = () => {
        fetchTables();
    };
    const handleLogout = () => {
        localStorage.removeItem("user");
        window.location.href = "/login";
    };

    const handlePayment = async (customerIdentifier) => {
        try {
            const payPromises = tables.flatMap((table) =>
                table.groupedOrders[customerIdentifier]?.map((order) =>
                    axios.put(`${API_URL}/orders/${order.id}`, { status: 'payed' })
                )
            );

            await Promise.all(payPromises);
            await fetchTables();
        } catch (error) {
            console.error('Error processing payment:', error);
        }
    };
    let toastTimer = null;
    const handleOrderUpdated = (updatedOrder) => {
        const { status, table_number } = updatedOrder;


        if (toastTimer) {
            clearTimeout(toastTimer);
        }

        // add table to pending notifications
        setPendingTableNotifications(prev => [...prev, { table_number, status }]);

        // set a new timer
        toastTimer = setTimeout(() => {
            // filter tables that have the same status
            const sameStatusTables = pendingTableNotifications.filter(
                table => table.status === status
            );

            if (!toastShownForTableStatus[`${table_number}_${status}`]) {
                // display toast only if it has not been shown for this table and status
                if (status === 'in progress') {
                    toast.info(`Items at table ${table_number} are updated: in progress`, {
                        position: toast.POSITION.TOP_LEFT,
                        autoClose: false,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                } else if (status === 'ready to serve') {
                    toast.success(`Items at table ${table_number} are updated: ready to serve`, {
                        position: toast.POSITION.TOP_LEFT,
                        autoClose: false,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }

                // mark this toast as shown for this table and status
                setToastShownForTableStatus(prev => ({
                    ...prev,
                    [`${table_number}_${status}`]: true
                }));
            }

            // reset pending notifications and the timer
            setPendingTableNotifications([]);
            toastTimer = null;
        }, 1000); // wait for 1 second

        fetchTables();
    };


    const handleOrderDeleted = (deletedOrderId) => {
        setTables((prevTables) => {
            const updatedTables = prevTables.map((table) => {
                let newGroupedOrders = { ...table.groupedOrders };

                const customerIdentifiers = Object.keys(table.groupedOrders);
                customerIdentifiers.forEach((customerIdentifier) => {
                    newGroupedOrders[customerIdentifier] = table.groupedOrders[customerIdentifier].filter(
                        (order) => order.id !== deletedOrderId
                    );
                });

                return {
                    ...table,
                    groupedOrders: newGroupedOrders,
                };
            });
            return updatedTables;
        });

    };



    return (
        <div className="waiter-page">
            <button onClick={handleLogout} className="logout-button">Logout</button>
            <div className="table-list">
                {tables.map((table, index) => (
                    <div
                        key={table.id}
                        className={`
                       table-container 
                       ${Object.keys(table.groupedOrders).length > 0 ? 'has-orders' : 'no-orders'} 
                       ${table.expanded ? 'expanded' : ''} 
                       ${animateTable && table.animate ? 'fade-out' : ''} 
                       ${animateTable && table.expanded ? 'center-table' : ''}
                   `}
                        onClick={() => {
                            toggleOrders(index);

                        }}
                    >

                        <h3 className='table-title'>Table {table.table_number}</h3>
                        <br />

                        {table.expanded && Object.keys(table.groupedOrders).length > 0 ? (

                            <div className="customer-group">
                                {Object.entries(table.groupedOrders).map(([customerIdentifier, customerOrders]) => (
                                    <div key={customerIdentifier} className="customer-group-item">
                                        <div className="group-header">
                                            <p className="customer-identifier">
                                                {customerIdentifier.split('@')[0]} <br />
                                                {customerOrders.every(order => order.status === 'confirmed') && <span className="confirmed-tag">All Orders Confirmed</span>}
                                            </p>
                                        </div>
                                        <ul className="waiter-page-order-list">
                                            {customerOrders.map(order => (
                                                <li key={order.id} className="order-item">
                                                    {order.quantity} x {order.menuItem.name} {order.remark ? '(' + order.remark + ')' : ''}, Status: {order.status}, Item Price: ${(order.total_price * 1).toFixed(2)}
                                                    {order.status === 'pending' && (
                                                        <button className="button" onClick={() => handleAllowModifications(order.id)}>Allow Modifications</button>
                                                    )}
                                                </li>
                                            ))}
                                            <li className="total-amount">
                                                Amount: ${calculateTotalAmount(customerOrders)}
                                                {customerOrders.every(order => order.status === 'confirmed' || order.status === 'in progress' || order.status === 'ready to serve') ? (null) : (

                                                    <button className="button" onClick={() => handleConfirmGroup(customerIdentifier)}>Confirm</button>

                                                )}
                                                {customerOrders.every(order => order.status === 'ready to serve') && (
                                                    <button className="button pay-button" onClick={() => handlePayment(customerIdentifier)}>Pay</button>
                                                )}
                                            </li>
                                        </ul>
                                    </div>
                                ))}
                                <div className="total-amount">
                                    Total Amount: ${table.totalAmount}
                                </div>
                            </div>
                        ) : <p className={`no-orders ${Object.keys(table.groupedOrders).length === 0 ? 'show' : 'hide'}`}>No active orders</p>}
                        {Object.keys(table.groupedOrders).length > 0 && <div className="order-count">{Object.keys(table.groupedOrders).length}</div>}
                    </div>

                ))
                }
            </div >
            <ToastContainer />
        </div >
    );
};

export default WaiterPage;
