import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import './Home.css';


function Home() {

    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        setIsLoaded(true);
    }, []);


    return (
        <div className={`home ${isLoaded ? 'loaded' : ''}`}>
            <div className="content">
                <h1>Welcome to Our Restaurant</h1>
                <Link to="/menu">View Menu</Link>
            </div>
        </div>
    );
}

export default Home;