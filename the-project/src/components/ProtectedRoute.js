import { useNavigate } from 'react-router-dom';
import { useAuth } from './AuthContext';

const ProtectedRoute = ({ children, roles }) => {
    const { currentUser } = useAuth();
    const navigate = useNavigate();

    if (!currentUser) {
        navigate('/login');
        return null;
    }

    if (roles && !roles.includes(currentUser.role)) {
        switch (currentUser.role) {
            case 1:
                navigate('/admin');
                break;
            case 2:
                navigate('/waiter');
                break;
            default:
                navigate('/login');
        }
        return null;
    }

    return children;
};

export default ProtectedRoute;
