import React, { createContext, useContext, useState, useEffect } from 'react';
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;

const AuthContext = createContext();

export const useAuth = () => {
    return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(null);
    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            axios.get(`${API_URL}/auth/me`, { headers: { Authorization: `Bearer ${token}` } })
                .then(response => {
                    console.log("RESPONSE:", response);
                    setCurrentUser(response.data.user);
                })
                .catch(error => {

                    localStorage.removeItem('token');


                });

        }
    }, []);
    const value = {
        currentUser,
        setCurrentUser,
    };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
