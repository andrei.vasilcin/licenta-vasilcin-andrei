import React, { useEffect, useState } from 'react';
import { BrowserRouter as Navigate, useNavigate } from 'react-router-dom';
import './OrderPage.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faBookOpen } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import socket from './socket';


const MAX_QUANTITY_PER_ITEM = 5;

const API_URL = process.env.REACT_APP_API_URL;



const getUniqueIdentifier = () => {
    const uniqueIdentifier = localStorage.getItem('uniqueIdentifier');
    return uniqueIdentifier;
};

const getTableNumber = () => {
    const tableNumber = localStorage.getItem('tableId');
    return tableNumber;
};

console.log("order page identifier:", getUniqueIdentifier());

const OrderPage = () => {
    const [loaded, setLoaded] = useState(false);
    const [unconfirmedOrdersItems, setUnconfirmedOrdersItems] = useState([]);
    const [confirmedOrdersItems, setConfirmedOrdersItems] = useState([]);
    const [pendingNotifications, setPendingNotifications] = useState([]);
    const [toastShownForStatus, setToastShownForStatus] = useState({});


    const navigate = useNavigate();
    const handleVisibilityChange = () => {
        if (document.visibilityState === 'visible') {
            if (!socket.connected) {
                socket.connect();
            }
            fetchConfirmedOrders();
        }
    };

    useEffect(() => {
        socket.on('welcome', (message) => {
            console.log('Received:', message);
        });

        socket.on('temporaryOrdersCreated', (newTemporaryOrders) => {
            if (window.location.pathname !== '/order') {
                return;
            }
            handleTemporaryOrderCreated(newTemporaryOrders);
        });
        socket.on('orderDeleted', (deletedOrders) => {
            if (window.location.pathname !== '/order') {
                return;
            }
            handleDeletedOrders(deletedOrders);
        });
        socket.on('ordersCreated', handleConfirmedOrdersUpdated);
        socket.on('orderUpdated', handleOrderUpdated);
        document.addEventListener('visibilitychange', handleVisibilityChange);
        const handleHelpRequest = () => {
            const tableNumber = getTableNumber();
            const uniqueIdentifier = getUniqueIdentifier();
            socket.emit('requestHelp', { tableNumber, uniqueIdentifier });
            toast.info(`Request for help sent for table ${tableNumber}.`, {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 2000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
        };
        window.handleHelpRequest = handleHelpRequest;
        fetchUnconfirmedOrders();
        fetchConfirmedOrders();

        return () => {

            socket.off('temporaryOrdersCreated', handleTemporaryOrderCreated);
            socket.off('orderDeleted', handleDeletedOrders);
            socket.off('ordersCreated', handleConfirmedOrdersUpdated);
            socket.off('orderUpdated', handleOrderUpdated);
            document.removeEventListener('visibilitychange', handleVisibilityChange);

        };

    }, []);

    let toastTimer = null;

    const handleOrderUpdated = (order) => {
        if (getUniqueIdentifier() !== order.customer_identifier) {
            return;
        }
        if (toastTimer) {
            clearTimeout(toastTimer);
        }
        toastTimer = setTimeout(() => {
            if (order.status === 'confirmed') {
                showToast('Order has been confirmed!', 'success');
            } else if (order.status === 'in progress') {
                showToast('Order is in progress!', 'info');
            } else if (order.status === 'ready to serve') {
                showToast('Order on your way!!', 'success');
            }
            toastTimer = null;

        }, 1000); // wait for 1 second

        fetchConfirmedOrders();
    };
    const showToast = (message, type) => {
        const toastOptions = {
            position: toast.POSITION.BOTTOM_CENTER,
            autoClose: false,
            hideProgressBar: true,
            closeButton: false,
            bodyClassName: "toast-body",
            className: "toast-container",
            theme: "colored",
        };

        if (type === 'success') {
            toast.success(message, toastOptions);
        } else if (type === 'info') {
            toast.info(message, toastOptions);
        } else if (type === 'error') {
            toast.error(message, toastOptions);
        } else if (type === 'warn') {
            toast.warn(message, toastOptions);
        }
    };

    const fetchUnconfirmedOrders = async () => {
        try {
            const fetchResponse = await axios.get(`${API_URL}/temporary-orders/customer_identifier/${getUniqueIdentifier()}`);
            setUnconfirmedOrdersItems(fetchResponse.data);
        } catch (error) {
            console.error('Error fetching unconfirmed orders:', error);
        }
    };
    const fetchConfirmedOrders = async () => {
        try {
            const fetchResponse = await axios.get(`${API_URL}/orders/confirmed/${getTableNumber()}`);
            setConfirmedOrdersItems(fetchResponse.data);
        } catch (error) {
            console.error('Error fetching confirmed orders:', error);
        }
    };

    const handleConfirmedOrdersUpdated = () => {
        fetchConfirmedOrders();
    };
    const handleTemporaryOrderCreated = () => {
        fetchUnconfirmedOrders();
    };
    const handleDeletedOrders = () => {
        fetchConfirmedOrders();
    };

    const handleRemoveItem = async (itemId) => {
        try {
            // find the item to be removed
            const itemToRemove = unconfirmedOrdersItems.find(item => item.id === itemId);
            if (itemToRemove) {
                await axios.delete(`${API_URL}/temporary-orders/${itemId}`);
                setUnconfirmedOrdersItems(prevItems => prevItems.filter(item => item.id !== itemId));
                toast.warn(`${itemToRemove.menuItem.name} removed from the order!`, {
                    position: toast.POSITION.BOTTOM_CENTER,
                    autoClose: 2000,
                    hideProgressBar: true,
                    closeButton: false,
                    bodyClassName: "toast-body",
                    className: "toast-container",
                    theme: "colored"
                });
            } else {
                console.warn('Item not found:', itemId);
            }
        } catch (error) {
            console.error('Error removing item:', error);
        }
    };



    const handleUpdateQuantity = async (itemId, newQuantity) => {
        // check if the new quantity exceeds the maximum allowed
        if (newQuantity > MAX_QUANTITY_PER_ITEM) {
            toast.warn(`You cannot order more than ${MAX_QUANTITY_PER_ITEM} of the same item.`, {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 3000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
            return;
        }

        if (newQuantity < 1) {
            newQuantity = 1;
        }
        try {
            const response = await axios.put(`${API_URL}/temporary-orders/${itemId}`, { quantity: newQuantity });
            setUnconfirmedOrdersItems(prevItems =>
                prevItems.map(item => (item.id === itemId ? { ...item, quantity: newQuantity } : item))
            );
            if (newQuantity != 1) {
                toast.info('Item quantity updated', {
                    position: toast.POSITION.BOTTOM_CENTER,
                    autoClose: 2000,
                    hideProgressBar: true,
                    closeButton: false,
                    bodyClassName: "toast-body",
                    className: "toast-container",
                    theme: "colored"
                });
            }

        } catch (error) {
            console.error('Error updating quantity:', error);
            if (error.response && error.response.data) {
                const serverMessage = error.response.data.message;
                toast.error("Sorry, the maximum item quantity reached");
            }
        }
    };
    const handleUpdateRemark = async (itemId, newRemark) => {
        try {
            await axios.put(`${API_URL}/temporary-orders/${itemId}`, { remark: newRemark });
            setUnconfirmedOrdersItems(prevItems =>
                prevItems.map(item => (item.id === itemId ? { ...item, remark: newRemark } : item))
            );
            toast.info('Your remark has been saved', {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 2000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
        } catch (error) {
            console.error('Error updating remark:', error);
        }
    };


    const handleCancelOrder = async () => {
        try {
            await axios.delete(`${API_URL}/temporary-orders/cancel/${getUniqueIdentifier()}`);
            setUnconfirmedOrdersItems([]);
            toast.error('Order canceled completly', {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 2000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
        } catch (error) {
            console.error('Error canceling order:', error);
        }
    };

    const handleConfirmOrder = async () => {
        try {
            await axios.post(`${API_URL}/orders/confirm-unconfirmed/${getUniqueIdentifier()}/${getTableNumber()}`);
            const fetchResponse = await axios.get(`${API_URL}/orders/confirmed/${getTableNumber()}`);
            setUnconfirmedOrdersItems([]);
            setConfirmedOrdersItems(fetchResponse.data);
            localStorage.removeItem('orderedItems');
            toast.success('You placed your order!', {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 2000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
        } catch (error) {
            console.error('Error confirming order:', error);
        }
    };


    const calculateTotalPrice = (items) => {
        return items.reduce((total, item) => total + item.total_price * item.quantity, 0).toFixed(2);
    };

    function groupByUniqueIdentifier(orders) {
        const groupedOrders = {};
        for (const order of orders) {
            const customerIdentifier = order.customer_identifier.split('@')[0];
            if (!groupedOrders.hasOwnProperty(customerIdentifier)) {
                groupedOrders[customerIdentifier] = [];
            }
            groupedOrders[customerIdentifier].push(order);
        }
        return groupedOrders;
    }

    const calculateTotalGroupPrice = (items) => {
        return items.reduce((total, item) => total + parseFloat(item.total_price * item.quantity), 0).toFixed(2);
    };

    const calculateTotalTablePrice = () => {
        return confirmedOrdersItems.reduce((total, item) => total + parseFloat(item.total_price * item.quantity), 0).toFixed(2);
    };

    return (
        <div className={`page ${loaded ? 'loaded' : ''}`}>
            <div className="order-page">
                <div className='order-container'>
                    <nav className="navigation-bar">
                        <Link to="/" className="navigation-button left">
                            <FontAwesomeIcon icon={faHome} />
                        </Link>
                        <button className="help-button" onClick={window.handleHelpRequest}>Request Waiter</button>
                        <Link to="/menu" className="navigation-button right ">
                            <FontAwesomeIcon icon={faBookOpen} />
                        </Link>
                    </nav>
                    {unconfirmedOrdersItems.length > 0 && (
                        <div className="unconfirmed-orders">
                            <h1 className="order-title">Your Order</h1>
                            <div className="order-list">
                                {unconfirmedOrdersItems.map((item) => (
                                    <div key={item.id} className="order-item">
                                        <img className="item-image" src={item.menuItem.image_url} alt={item.menuItem.name} />
                                        <div className="item-details">
                                            <span className="item-name">{item.menuItem.name}</span>
                                            <span className="item-price">${item.total_price}</span>
                                        </div>
                                        <div className="item-controls">
                                            <button className="quantity-button-low" onClick={() => handleUpdateQuantity(item.id, item.quantity - 1)}>-</button>
                                            <span className="item-quantity">{item.quantity}</span>
                                            <button className="quantity-button-high" onClick={() => handleUpdateQuantity(item.id, item.quantity + 1)}>+</button>
                                            <button className="remove-button" onClick={() => handleRemoveItem(item.id)}>Remove</button>
                                            <textarea
                                                placeholder="Add a remark..."
                                                defaultValue={item.remark}
                                                onBlur={(e) => handleUpdateRemark(item.id, e.target.value)}
                                                className="order-remark-textarea"
                                            />
                                        </div>
                                    </div>
                                ))}
                            </div>
                            <div className="total-container">
                                <span className="total-label">Total:</span>
                                <span className="total-price">${calculateTotalPrice(unconfirmedOrdersItems)}</span>
                            </div>
                            <div className="order-actions">
                                <button className="cancel-button" onClick={handleCancelOrder}>Cancel Order</button>
                                <button className="confirm-button" onClick={handleConfirmOrder}>Confirm Order</button>
                            </div>
                        </div>

                    )}
                    {confirmedOrdersItems.length > 0 && (
                        <div className="confirmed-orders">
                            <h2 className="confirmed-orders-title">Table Orders</h2>
                            <div className="confirmed-orders-list">
                                {Object.entries(groupByUniqueIdentifier(confirmedOrdersItems)).map(([customer_identifier, items]) => (
                                    <div key={customer_identifier} className="confirmed-order-group">
                                        <div className="confirmed-order-name">{customer_identifier.split('@')[0]}</div>
                                        {items.map((item) => (
                                            <div key={item.menu_item_id} className="confirmed-suborder">
                                                <img className="confirmed-suborder-image" src={item.menuItem.image_url} alt={item.menuItem.name} />
                                                <div className="confirmed-suborder-details">
                                                    <span className="confirmed-suborder-name">
                                                        {item.menuItem.name}
                                                    </span>
                                                    <span className="item-price-confirmed">{item.quantity} x ${item.total_price}</span>
                                                    <span className="item-status-confirmed">Status:  {item.status}</span>
                                                </div>
                                                <div className="confirmed-suborder-price-container">
                                                    <span className="confirmed-suborder-price">${(item.total_price * item.quantity).toFixed(2)}</span>
                                                </div>
                                            </div>
                                        ))}
                                        <div className="total-order-price">
                                            Total Price: ${calculateTotalGroupPrice(items)}
                                        </div>
                                    </div>
                                ))}
                                <div className="total-order-price">
                                    Total Table Order Price: ${calculateTotalTablePrice()}
                                </div>
                            </div>
                        </div>

                    )}
                    {unconfirmedOrdersItems.length === 0 && confirmedOrdersItems.length === 0 && (
                        <div className='no-orders-message'>
                            No orders available.
                        </div>
                    )}
                </div>
            </div>
            <ToastContainer />
        </div>
    );
};

export default OrderPage;
