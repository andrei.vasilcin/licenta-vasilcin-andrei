
import React, { createContext, useContext, useState, useEffect } from 'react';
import { generateInitialUniqueId, updateUniqueIdWithNickname, generateGuestUniqueId } from '../helpers/helpers';

export const UniqueIdContext = createContext();

export const useUniqueId = () => {
    return useContext(UniqueIdContext);
}

export const UniqueIdProvider = ({ children }) => {
    const initialId = localStorage.getItem('uniqueIdentifier') || generateInitialUniqueId();
    console.log("Inside UniqueIdProvider - initialId:", initialId);
    const [uniqueIdentifier, setUniqueIdentifierInternal] = useState(initialId);


    useEffect(() => {
        localStorage.setItem('uniqueIdentifier', uniqueIdentifier);
    }, [uniqueIdentifier]);


    const setUniqueIdentifier = (newId) => {
        if (!uniqueIdentifier.includes('@')) {
            setUniqueIdentifierInternal(newId);
        }
    }



    return (
        <UniqueIdContext.Provider value={{ uniqueIdentifier, setUniqueIdentifier }}>
            {children}
        </UniqueIdContext.Provider>
    );
}
