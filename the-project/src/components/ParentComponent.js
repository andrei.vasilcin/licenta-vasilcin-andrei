import React, { useState, useEffect, useContext } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate, useNavigate } from 'react-router-dom';
import Home from './Home';
import MenuComponent from './MenuComponent';
import OrderPage from './OrderPage';
import WelcomePage from './WelcomePage';
import Scan from './Scan';
import Login from './Login';
import KitchenDashboard from './KitchenDashboard';
import AdminDashboard from './AdminDashboard';
import WaiterPage from './WaiterPage';
import { useUniqueId } from './UniqueIdContext';
import { useAuth } from './AuthContext';
import ProtectedRoute from './ProtectedRoute';
import UnprotectedRoute from './UnprotectedRoute';



const ParentComponent = () => {
    const { uniqueIdentifier } = useUniqueId();
    const { currentUser, setCurrentUser } = useAuth();

    const navigate = useNavigate();
    useEffect(() => {
        const storedUser = localStorage.getItem('user');
        if (storedUser && (window.location.pathname === '/waiter' || window.location.pathname === '/kitchen' || window.location.pathname === '/admin')) {
            setCurrentUser(JSON.parse(storedUser));
        } else if (window.location.pathname !== '/waiter' || window.location.pathname !== '/kitchen' || window.location.pathname !== '/admin') {

            navigate(window.location.pathname);

        } else {
            navigate('/login')
        }

    }, []);

    const handleLogin = (user) => {
        setCurrentUser(user);
        localStorage.setItem('user', JSON.stringify(user));
        if (user && user.role) {
            switch (user.role) {
                case 1:
                    navigate('/admin');
                    break;
                case 2:
                    navigate('/waiter');
                    break;
                case 3:
                    navigate('/kitchen');
                    break;
                default:
                    navigate('/login');
            }
        } else {
            navigate('/login');

        }

    };


    return (
        <div className="App">
            <Routes>
                <Route
                    path="/"
                    element={
                        uniqueIdentifier && uniqueIdentifier.includes('@') ? (
                            <UnprotectedRoute><Home /></UnprotectedRoute>
                        ) : (
                            <Navigate to="/scan" state={{ message: "Please scan the QR Code available on the table" }} />
                        )
                    }
                />
                <Route
                    path="/menu"
                    element={
                        uniqueIdentifier && uniqueIdentifier.includes('@') ? (
                            <UnprotectedRoute><MenuComponent uniqueIdentifier={uniqueIdentifier} /></UnprotectedRoute>
                        ) : (
                            <Navigate to="/scan" state={{ message: "Please scan the QR Code available on the table" }} />
                        )
                    }
                />
                <Route
                    path="/order"
                    element={
                        uniqueIdentifier && uniqueIdentifier.includes('@') ? (
                            <UnprotectedRoute><OrderPage uniqueIdentifier={uniqueIdentifier} /></UnprotectedRoute>
                        ) : (
                            <Navigate to="/scan" state={{ message: "Please scan the QR Code available on the table" }} />
                        )
                    }
                />

                <Route path="/welcome/:table_number" element={<UnprotectedRoute><WelcomePage /></UnprotectedRoute>} />
                <Route path="/scan" element={<UnprotectedRoute><Scan /></UnprotectedRoute>} />
                <Route path="/waiter" element={<ProtectedRoute roles={[1, 2]}><WaiterPage /></ProtectedRoute>} />
                <Route path="/kitchen" element={<ProtectedRoute roles={[1, 3]}><KitchenDashboard /></ProtectedRoute>} />

                <Route path="/admin" element={<ProtectedRoute roles={[1]}><AdminDashboard /></ProtectedRoute>} />
                <Route path="/login" element={<Login onLogin={handleLogin} />} />
            </Routes>
        </div>
    );
};

export default ParentComponent;

