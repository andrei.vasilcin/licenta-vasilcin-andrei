const API_URL = process.env.REACT_APP_API_URL;
const entityConfigs = {
    Categories: {
        attributes: [{
            key: 'name',
            label: 'Name',
            type: 'text',
            placeholder: 'Enter category name'
        },
        {
            key: 'description',
            label: 'Description',
            type: 'text',
            placeholder: 'Enter category description'
        }],
        endpoints: {
            create: `${API_URL}/categories`,
            read: `${API_URL}/categories`,
            update: `${API_URL}/categories`,
            delete: `${API_URL}/categories`,
        }
    },

    MenuItems: {
        attributes: [{
            key: 'name',
            label: 'Menu Item Name',
            type: 'text',
            placeholder: 'Enter menu item name'
        },
        {
            key: 'description',
            label: 'Description',
            type: 'text',
            placeholder: 'Enter menu item description'
        },
        {
            key: 'price',
            label: 'Price',
            type: 'number',
            inputProps: {
                min: '0',
                step: '0.01',
            },
            placeholder: 'Enter menu item price'
        },
        {
            key: 'image_url',
            label: 'Image',
            type: 'file',
            placeholder: 'Upload menu item image',
        },
        {
            key: 'category_id',
            label: 'Category',
            type: 'select',
            placeholder: 'Set the menu item category'
        },
        {
            key: 'availability',
            label: 'Availability',
            type: 'text',
            placeholder: 'Set the menu item availability'
        }],
        endpoints: {
            create: `${API_URL}/menu-items`,
            read: `${API_URL}/menu-items`,
            update: `${API_URL}/menu-items`,
            delete: `${API_URL}/menu-items`,
        }
    },
    Users: {
        attributes: [
            {
                key: 'username',
                label: 'Username',
                type: 'text',
                placeholder: 'Enter username'
            },
            {
                key: 'password',
                label: 'Password',
                type: 'password',
                placeholder: 'Enter password'
            },
            {
                key: 'role',
                label: 'Role',
                type: 'select',
                placeholder: 'Select role',
            },
        ],
        endpoints: {
            create: `${API_URL}/users`,
            read: `${API_URL}/users`,
            update: `${API_URL}/users`,
            delete: `${API_URL}/users`,
        }
    },
    Roles: {
        attributes: [
            {
                key: 'name',
                label: 'Role Name',
                type: 'text',
                placeholder: 'Enter the role name'
            },
            {
                key: 'description',
                label: 'Role Description',
                type: 'text',
                placeholder: 'Enter the role description'
            },
        ],
        endpoints: {
            create: `${API_URL}/roles`,
            read: `${API_URL}/roles`,
            update: `${API_URL}/roles`,
            delete: `${API_URL}/roles`,
        }
    },
    Ingredients: {
        attributes: [
            {
                key: 'name',
                label: 'Ingredient Name',
                type: 'text',
                placeholder: 'Enter the ingredient name'
            },
            {
                key: 'unit',
                label: 'Units',
                type: 'text',
                placeholder: 'gr or kg'
            },
            {
                key: 'quantity',
                label: 'Quantity',
                type: 'number',
                placeholder: 'Enter the quantity',
                inputProps: {
                    min: '0',
                    step: '0.01',
                },

            },
            {
                key: 'last_restok_date',
                label: 'Last Restock date',
                type: 'date',
                placeholder: 'Set the last restock date'
            },
            {
                key: 'expiry_date',
                label: 'The Expiration date',
                type: 'date',
                placeholder: 'Add the expiration date'
            },
        ],
        endpoints: {
            create: `${API_URL}/ingredients`,
            read: `${API_URL}/ingredients`,
            update: `${API_URL}/ingredients`,
            delete: `${API_URL}/ingredients`,
        }
    },
    MenuItemIngredients: {
        attributes: [
            {
                key: 'quantity_required',
                label: 'Quantity required',
                type: 'number',
                placeholder: 'Set the quantity required',
                inputProps: {
                    min: '0',
                    step: '0.01',
                },
            },
            {
                key: 'menu_item_id',
                label: 'Menu Item',
                type: 'select',
                placeholder: 'Set the Menu Item'
            },
            {
                key: 'ingredient_id',
                label: 'Ingredient required',
                type: 'select',
                placeholder: 'Choose the required ingredient',

            },
        ],
        endpoints: {
            create: `${API_URL}/menu-item-ingredients`,
            read: `${API_URL}/menu-item-ingredients`,
            update: `${API_URL}/menu-item-ingredients`,
            delete: `${API_URL}/menu-item-ingredients`,
        }
    },
    Tables: {
        attributes: [
            {
                key: 'table_number',
                label: 'Table Number',
                type: 'number',
                placeholder: 'Set the Table number',
                inputProps: {
                    min: '0',
                    step: '0.01',
                },
            },
            {
                key: 'capacity',
                label: 'Table Capacity',
                type: 'number',
                placeholder: 'Set Table Capacity',
                inputProps: {
                    min: '0',
                    step: '0.01',
                },
            },
            {
                key: 'status',
                label: 'Set the Table status',
                type: 'select',
                placeholder: 'Set the Table Status',

            },
        ],
        endpoints: {
            create: `${API_URL}/tables`,
            read: `${API_URL}/tables`,
            update: `${API_URL}/tables`,
            delete: `${API_URL}/tables`,
        }
    },
};

export default entityConfigs;
