import React, { useCallback } from 'react';

const CRUDModal = ({ entityType, action, data, onSubmit, onClose, entityConfig, setData, errors = {}, clearError, categoryOptions, roles, menuItems, ingredients }) => {
    console.log("modal data:", data);
    const handleFileChange = (e, attributeKey) => {
        const file = e.target.files[0];
        const newData = { ...data, [attributeKey]: file };
        setData(newData);
    };
    const handlePriceChange = e => {
        const value = e.target.value;
        if (value >= 0) { // check that the value is non-negative
            const newData = { ...data, price: value };
            setData(newData);
            clearError('price');
        }
    };
    const handleInputChange = useCallback((e, attributeKey) => {
        const newData = { ...data, [attributeKey]: e.target.value };
        setData(newData);
        if (e.target.value) {
            clearError(attributeKey);
        }
    }, [data, setData, clearError]);


    return (
        <div className={`crud-modal-overlay ${action ? "show" : ""}`}>
            <div className={`crud-modal ${action ? "show" : ""}`}>
                <h2>{`${action} ${entityType}`}</h2>

                {entityConfig.attributes.map(attribute => (
                    <div className="modal-input-container" key={attribute.key}>
                        <label className="modal-label">{attribute.label}</label>
                        {attribute.key === 'category_id' ? (
                            <select
                                name="category_id"
                                value={data.category_id || ''}
                                onChange={e => {

                                    const newData = { ...data, category_id: e.target.value };
                                    setData(newData);
                                    clearError('category_id');
                                }}
                            >
                                <option value="">Select Category</option>
                                {categoryOptions.map(category => (
                                    <option key={category.id} value={category.id}>
                                        {category.name}
                                    </option>
                                ))}
                            </select>
                        ) : attribute.key === 'role' ? (
                            <select
                                name="roles"
                                value={data.role || ''}
                                onChange={e => {

                                    const newData = { ...data, role: e.target.value };
                                    setData(newData);
                                    clearError('role');
                                }}
                            >
                                <option value="">Select Role</option>
                                {roles.map(role => (
                                    <option key={role.id} value={role.id}>
                                        {role.name}
                                    </option>
                                ))}
                            </select>
                        ) : attribute.key === 'menu_item_id' ? (
                            <select
                                name="menuItems"
                                value={data.menu_item_id || ''}
                                onChange={e => {

                                    const newData = { ...data, menu_item_id: e.target.value };
                                    setData(newData);
                                    clearError('menuItem');
                                }}
                            >
                                <option value="">Select Menu Item</option>
                                {menuItems.map(menuItem => (
                                    <option key={menuItem.id} value={menuItem.id}>
                                        {menuItem.name}
                                    </option>
                                ))}
                            </select>
                        ) : attribute.key === 'ingredient_id' ? (
                            <select
                                name="ingredients"
                                value={data.ingredient_id || ''}
                                onChange={e => {

                                    const newData = { ...data, ingredient_id: e.target.value };
                                    setData(newData);
                                    clearError('ingredient');
                                }}
                            >
                                <option value="">Select the Ingredient</option>
                                {ingredients.map(ingredient => (
                                    <option key={ingredient.id} value={ingredient.id}>
                                        {ingredient.name}
                                    </option>
                                ))}
                            </select>
                        ) : attribute.key === 'status' ? (
                            <select
                                name="status"
                                value={data.status || ''}
                                onChange={e => {

                                    const newData = { ...data, status: e.target.value };
                                    setData(newData);
                                    clearError('availability');
                                }}
                            >
                                <option value="">Select Availability</option>
                                <option value="available">Available</option>
                                <option value="unavailable">Unavailable</option>
                                <option value="reserved">Reserved</option>
                                <option value="occupied">Occupied</option>


                            </select>
                        ) : attribute.key === 'availability' ? (
                            <select
                                name="availability"
                                value={data.availability || ''}
                                onChange={e => {

                                    const newData = { ...data, availability: e.target.value };
                                    setData(newData);
                                    clearError('availability');
                                }}
                            >
                                <option value="">Select Availability</option>
                                <option value="available">Available</option>
                                <option value="unavailable">Unavailable</option>
                            </select>
                        ) : attribute.type === 'file' ? (
                            <input className="modal-input"
                                type="file"
                                name={attribute.key}
                                onChange={e => handleFileChange(e, attribute.key)}
                            />
                        ) : attribute.key === 'price' ? (
                            <input className="modal-input"
                                type="number"
                                name="price"
                                value={data.price || ''}
                                placeholder={attribute.placeholder}
                                onChange={handlePriceChange}
                                {...attribute.inputProps}
                            />
                        ) : (
                            <input
                                className="modal-input"
                                type={attribute.type}
                                name={attribute.key}
                                value={data[attribute.key] || ''}
                                placeholder={attribute.placeholder}
                                onChange={e => handleInputChange(e, attribute.key)}
                                {...attribute.inputProps}
                            />
                        )}
                        {attribute && errors && errors[attribute.key] && <span className="error-message">{errors[attribute.key]}</span>}
                    </div>
                ))}

                <button className="admin-content-button" onClick={() => onSubmit(data)}>Submit</button>
                <button className="admin-content-button" onClick={onClose}>Close</button>
            </div>
        </div>
    );
};

export default CRUDModal;
