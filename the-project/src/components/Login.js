import React, { useState } from 'react';
import axios from 'axios';
import './Login.css';

const API_URL = process.env.REACT_APP_API_URL;

const Login = ({ onLogin }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError(null);
        try {
            const response = await axios.post(`${API_URL}/auth/login`, {
                username,
                password,
            });
            const token = response.data.token;
            if (token) {
                localStorage.setItem('token', token);
            }

            const user = response.data.user;
            localStorage.setItem('user', JSON.stringify(user));

            onLogin(user);

        } catch (err) {
            console.log("err:", err);
            setError('Invalid username or password');
        }
    };

    return (
        <div className="login-container">
            <h1 className="login-title">Login</h1>
            <form onSubmit={handleSubmit}>
                <div className="login-field">
                    <label htmlFor="username" className="login-label">Username:</label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        className="login-input"
                    />
                </div>
                <div className="login-field">
                    <label htmlFor="password" className="login-label">Password:</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        className="login-input"
                    />
                </div>
                {error && <p className="error-message">{error}</p>}
                <div className="login-field">
                    <button type="submit" className="login-button">Login</button>
                </div>
            </form>
        </div>

    );
};

export default Login;
