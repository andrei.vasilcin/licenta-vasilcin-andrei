import { LineChart, Line, XAxis, YAxis, Tooltip, Legend, CartesianGrid } from 'recharts';

const OrdersOverTimeChart = ({ data }) => (
    <LineChart width={1000} height={500} data={data}>
        <Line type="monotone" dataKey="count" stroke="#8884d8" />
        <CartesianGrid stroke="#ccc" />
        <XAxis dataKey="date" />
        <YAxis />
        <Tooltip />
    </LineChart>
);

export default OrdersOverTimeChart;
