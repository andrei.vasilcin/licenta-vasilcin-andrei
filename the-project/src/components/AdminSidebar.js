import React from 'react';
import entityConfigs from './entityConfigs';


const AdminSidebar = ({ setActiveEntity, showStatistics, onEntitySelected }) => {
    const handleEntityClick = (entity) => {
        setActiveEntity(entity);
        onEntitySelected();
    };
    const handleLogout = () => {
        localStorage.removeItem("user");
        window.location.href = "/login";
    };
    return (
        <div className="admin-sidebar">
            {Object.keys(entityConfigs).map(entity => (
                <button key={entity} onClick={() => handleEntityClick(entity)}>
                    {entity}
                </button>
            ))}
            <button onClick={showStatistics}>Statistics and Reports</button>
            <button onClick={handleLogout}>Logout</button>
        </div>
    );
};

export default AdminSidebar;
