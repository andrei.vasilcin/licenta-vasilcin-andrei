import io from 'socket.io-client';

const socket = io('https://9ced-109-96-45-187.ngrok-free.app', {
    withCredentials: true,
    extraHeaders: {
        'ngrok-skip-browser-warning': 'true',
    },
});

export default socket;
