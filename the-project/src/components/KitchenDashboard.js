
import React, { useState, useEffect } from 'react';
import './KitchenDashboard.css';
import axios from 'axios';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import socket from './socket';


const API_URL = process.env.REACT_APP_API_URL;

const KitchenDashboard = () => {

    function groupItemsByIdAndRemark(orders) {
        const itemGroups = {};
        for (const order of orders) {
            const key = `${order.menu_item_id}-${order.remark || 'no-remark'}`;
            if (!itemGroups[key]) {
                itemGroups[key] = [];
            }
            itemGroups[key].push(order);
        }
        return itemGroups;
    }

    const [orders, setOrders] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [currentTime, setCurrentTime] = useState(new Date());


    useEffect(() => {
        const timerInterval = setInterval(() => {
            setCurrentTime(new Date());
        }, 1000);

        return () => clearInterval(timerInterval);
    }, []);

    useEffect(() => {




        fetchOrders();

        socket.on('orderUpdated', () => {
            fetchOrders();
        });

        return () => socket.off('orderUpdated', fetchOrders);
    }, []);

    const fetchOrders = async () => {
        try {
            const response = await axios.get(`${API_URL}/orders/all/confirmed`);
            setOrders(response.data);
        } catch (error) {
            console.error('Error fetching orders:', error);
        }
    };

    const groupedOrders = orders.reduce((acc, order) => {
        if (!acc[order.table_number]) {
            acc[order.table_number] = [];
        }
        acc[order.table_number].push(order);
        return acc;
    }, {});

    const tableNumbers = Object.keys(groupedOrders).filter(tableNumber => tableNumber.includes(searchTerm));

    const getTimeSinceOrderConfirmed = (orderTime) => {
        const orderDate = new Date(orderTime);
        const secondsDiff = Math.floor((currentTime - orderDate) / 1000);
        const minutes = Math.floor(secondsDiff / 60);
        const seconds = secondsDiff % 60;
        return `${minutes}m ${seconds}s`;
    };
    const handleGroupOrderStatusUpdate = async (table, status) => {
        try {
            const ordersToUpdate = groupedOrders[table];
            for (const order of ordersToUpdate) {
                await axios.put(`${API_URL}/orders/${order.id}`, { status });
            }
            fetchOrders();  // fetch updated orders after status change
        } catch (error) {
            console.error('Error updating order status for the group:', error);
        }
    };

    const getGroupOrderStatus = (orders) => {
        if (orders.every(order => order.status === 'confirmed')) {
            return 'confirmed';
        }
        if (orders.every(order => order.status === 'in progress')) {
            return 'in progress';
        }
        return 'mixed';
    };

    const getEarliestOrderTime = (orders) => {
        return Math.min(...orders.map(order => new Date(order.order_time)));
    };

    const handleOrderStatusUpdate = async (orderId, status) => {
        try {
            await axios.put(`${API_URL}/orders/${orderId}`, { status });
            fetchOrders(); // fetch updated orders after status change
        } catch (error) {
            console.error('Error updating order status:', error);
        }
    };
    const handleLogout = () => {
        localStorage.removeItem("user");
        window.location.href = "/login";
    };
    return (
        <div className="kitchen-dashboard">
            <header>
                Kitchen Dashboard


            </header>
            <div className="controls">
                <input
                    type="text"
                    placeholder="Search by table number..."
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
            </div>
            <TransitionGroup className="orders">

                {tableNumbers.map(table => (
                    <CSSTransition
                        key={table}
                        timeout={500}
                        classNames="order-transition"
                    >
                        <div className="order-card">
                            <div className="order-header">
                                <span className="table-number">Table: {table}</span>
                                <span className="order-time">Time since earliest order: {getTimeSinceOrderConfirmed(getEarliestOrderTime(groupedOrders[table]))}</span>
                            </div>

                            <ul className="order-items">
                                {Object.entries(groupItemsByIdAndRemark(groupedOrders[table])).map(([key, groupedItems]) => (
                                    <React.Fragment key={key}>
                                        <li className="item-entry">
                                            <span className="item-name">{groupedItems[0].menuItem.name}</span>
                                            {groupedItems[0].remark && (
                                                <span className="item-remark">
                                                    Remark: {groupedItems[0].remark}
                                                </span>
                                            )}
                                            <span className="item-quantity">x {groupedItems.length}</span>
                                        </li>
                                        {groupedItems.map((order) => (
                                            <li key={order.id} className="item-entry">
                                                {order.status === 'confirmed' && (
                                                    <button className='button-kitchen' onClick={() => handleOrderStatusUpdate(order.id, 'in progress')}>Start</button>
                                                )}
                                                {order.status === 'in progress' && (
                                                    <button className='button-kitchen' onClick={() => handleOrderStatusUpdate(order.id, 'ready to serve')}>Ready</button>
                                                )}
                                            </li>
                                        ))}
                                    </React.Fragment>
                                ))}
                            </ul>


                            <div className="order-actions">
                                {getGroupOrderStatus(groupedOrders[table]) === 'confirmed' && (
                                    <button className='button-kitchen' onClick={() => handleGroupOrderStatusUpdate(table, 'in progress')}>Start All</button>
                                )}
                                {getGroupOrderStatus(groupedOrders[table]) === 'in progress' && (
                                    <button className='button-kitchen' onClick={() => handleGroupOrderStatusUpdate(table, 'ready to serve')}>Ready All</button>
                                )}
                            </div>
                        </div>
                    </CSSTransition>
                ))}
            </TransitionGroup>
            <button onClick={handleLogout} className="button-logout">Logout</button>

        </div>
    );

};

export default KitchenDashboard;