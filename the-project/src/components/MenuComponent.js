import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './MenuComponent.css';

import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faBasketShopping } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

import socket from './socket';




const API_URL = process.env.REACT_APP_API_URL;

const MenuComponent = () => {
    const [menuItems, setMenuItems] = useState([]);
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState('');
    const [loaded, setLoaded] = useState(false);
    const [totalItems, setTotalItems] = useState(0);
    const [orderedItems, setOrderedItems] = useState([]);
    const [orderStatus, setOrderStatus] = useState('');



    useEffect(() => {
        fetchMenuItems();
        fetchCategories();
        fetchTotalItems();
        fetchOrderedItems();

        console.log("Connecting socket");
        socket.on('welcome', (message) => {
            console.log('Received:', message);
        });

        const handleHelpRequest = () => {
            const tableNumber = setTableId();
            const uniqueIdentifier = getUniqueIdentifier();
            socket.emit('requestHelp', { tableNumber, uniqueIdentifier });
            toast.info(`Request for help sent for table ${tableNumber}.`, {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 2000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
        };

        window.handleHelpRequest = handleHelpRequest;

        socket.on('orderUpdated', handleOrderUpdated);
        return () => {


            socket.off('orderUpdated', handleOrderUpdated);


        };

    }, []);

    useEffect(() => {
        setTimeout(() => {
            setLoaded(true);
        }, 1000);
    }, []);
    let toastTimer = null;

    const handleOrderUpdated = (order) => {
        if (getUniqueIdentifier() !== order.customer_identifier) {
            return;
        }


        if (toastTimer) {
            clearTimeout(toastTimer);
        }

        // set a new timer
        toastTimer = setTimeout(() => {
            if (order.status === 'confirmed') {
                showToast('Order has been confirmed!', 'success');
            } else if (order.status === 'in progress') {
                showToast('Order is in progress!', 'info');
            } else if (order.status === 'ready to serve') {
                showToast('Order on your way!!', 'success');
            }

            // reset the timer
            toastTimer = null;

        }, 1000); // wait for 1 second


    };

    const showToast = (message, type) => {
        const toastOptions = {
            position: toast.POSITION.BOTTOM_CENTER,
            autoClose: false,
            hideProgressBar: true,
            closeButton: false,
            bodyClassName: "toast-body",
            className: "toast-container",
            theme: "colored",
        };

        if (type === 'success') {
            toast.success(message, toastOptions);
        } else if (type === 'info') {
            toast.info(message, toastOptions);
        } else if (type === 'error') {
            toast.error(message, toastOptions);
        } else if (type === 'warn') {
            toast.warn(message, toastOptions);
        }
    };


    const getUniqueIdentifier = () => {
        const uniqueIdentifier = localStorage.getItem('uniqueIdentifier');
        return uniqueIdentifier;
    };

    const setTableId = () => {
        const tableId = localStorage.getItem('tableId');
        return tableId;
    };

    const fetchCategories = async () => {
        try {
            const response = await axios.get(`${API_URL}/categories`);
            setCategories(response.data);
        } catch (error) {
            console.error('Error fetching categories:', error);
        }
    };

    const fetchMenuItems = async () => {
        try {
            const response = await axios.get(`${API_URL}/menu-items`);
            setMenuItems(response.data);
            setLoaded(true);
        } catch (error) {
            console.error('Error fetching menu items:', error);
        }
    };

    const fetchTotalItems = async () => {
        try {
            const response = await axios.get(`${API_URL}/temporary-orders/customer_identifier/${getUniqueIdentifier()}`);
            setTotalItems(response.data);
        } catch (error) {
            console.error('Error fetching total items:', error);
        }
    };
    const fetchOrderedItems = async () => {
        try {
            const response = await axios.get(`${API_URL}/temporary-orders/customer_identifier/${getUniqueIdentifier()}`);
            setOrderedItems(response.data);
        } catch (error) {
            console.error('Error fetching ordered items:', error);
        }
    };


    const handleCategoryClick = (categoryName) => {
        setSelectedCategory((prevCategory) =>
            prevCategory === categoryName ? '' : categoryName
        );
    };


    const handleAddToOrder = async (menuItem, event) => {
        const rect = event.target.getBoundingClientRect();
        const top = rect.top + window.scrollY;
        const left = rect.left + window.scrollX;
        // condition to check if the menu item is available
        if (menuItem.availability !== 'available') {
            toast.warning(`Sorry, ${menuItem.name} is currently unavailable.`, {
                position: toast.POSITION.BOTTOM_CENTER,
                autoClose: 3000,
                hideProgressBar: true,
                closeButton: false,
                bodyClassName: "toast-body",
                className: "toast-container",
                theme: "colored"
            });
            return;
        }
        try {
            const existingItem = orderedItems.find(item => item.menu_item_id === menuItem.id);

            if (existingItem) {
                // quantity is already 5 or more?
                if (existingItem.quantity >= 5) {
                    toast.warning(`Maximum 5 ${menuItem.name} can be ordered!`, {
                        position: toast.POSITION.BOTTOM_CENTER,
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeButton: false,
                        bodyClassName: "toast-body",
                        className: "toast-container",
                        theme: "colored"
                    });
                } else {
                    // update the quantity in the orderedItems state
                    const updatedItems = orderedItems.map(item => {
                        if (item.menu_item_id === menuItem.id) {
                            return { ...item, quantity: item.quantity + 1 };
                        }
                        return item;
                    });
                    setOrderedItems(updatedItems);

                    // update the quantity in the backend
                    await axios.put(`${API_URL}/temporary-orders/${existingItem.id}`, {
                        quantity: existingItem.quantity + 1,
                    });


                    // fetch total items after updating the item in the order
                    await fetchTotalItems();


                    toast.info(`Added another ${menuItem.name} to your order!`, {
                        position: toast.POSITION.BOTTOM_CENTER,
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeButton: false,
                        bodyClassName: "toast-body",
                        className: "toast-container",
                        theme: "colored"
                    });
                }
            } else {


                const response = await axios.post(`${API_URL}/temporary-orders`, {
                    table_number: setTableId(),
                    customer_identifier: getUniqueIdentifier(),
                    total_price: menuItem.price,
                    status: 'temporary',
                    order_time: new Date(),
                    menu_item_id: menuItem.id,
                    quantity: 1,
                });
                toast.success(`Added ${menuItem.name} to your order!`, {
                    position: toast.POSITION.BOTTOM_CENTER,
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeButton: false,
                    bodyClassName: "toast-body",
                    className: "toast-container",
                    theme: "colored"
                });

                //fFetch updated ordered items after adding an item to the order
                await fetchOrderedItems();
                await fetchTotalItems();
            }
        } catch (error) {
            let message = error;
            if (message) {
                toast.error(`Sorry, ${menuItem.name} is out of order!`, {
                    position: toast.POSITION.BOTTOM_CENTER,
                    autoClose: 3000,
                    hideProgressBar: true,
                    closeButton: false,
                    bodyClassName: "toast-body",
                    className: "toast-container",
                    theme: "colored"
                });
            }

            console.error('Error adding item to temporary order:', error);
        }
    };



    const filteredMenuItems = selectedCategory
        ? menuItems.filter((item) => item.category_id === selectedCategory)
        : menuItems;

    return (
        <div>
            <div className={`page ${loaded ? 'loaded' : ''}`}>
                <div className='menu-page'>
                    <div className="menu-container">
                        <nav className="navigation-bar">
                            <Link to="/" className="navigation-button left">
                                <FontAwesomeIcon icon={faHome} />
                            </Link>
                            <button className="help-button" onClick={window.handleHelpRequest}>Request Waiter</button>
                            <Link to="/order" className={`navigation-button right ${totalItems.length > 0 ? 'has-items' : ''}`}>
                                <FontAwesomeIcon icon={faBasketShopping} style={{ color: "#ffffff" }} />
                                {totalItems.length > 0 && (
                                    <span className="order-bubble">{totalItems.length}</span>
                                )}
                            </Link>
                        </nav>

                        <h1 className="menu-title">Menu</h1>

                        <div className="category-tiles">
                            {categories.map((category) => (
                                <div
                                    key={category.id}
                                    className={`category-tile ${selectedCategory === category.id ? 'active' : ''}`}
                                    onClick={() => handleCategoryClick(category.id)}
                                >
                                    {category.name}
                                </div>
                            ))}
                        </div>


                        <TransitionGroup
                            component="ul"
                            className="menu-list menu-list-transition"
                        >
                            {filteredMenuItems.map((menuItem) => (
                                <CSSTransition
                                    key={menuItem.id}
                                    classNames="menu-item"
                                    timeout={300}
                                >
                                    <li className="menu-item">
                                        <img
                                            className="menu-item-image"
                                            src={menuItem.image_url}
                                            alt={menuItem.name}
                                        />
                                        <div className="menu-item-details">
                                            <h3 className="menu-item-name">
                                                {menuItem.name}
                                            </h3>
                                            <p className="menu-item-description">
                                                {menuItem.description}
                                            </p>
                                            <p className="menu-item-price">
                                                ${menuItem.price}
                                            </p>
                                        </div>
                                        <button
                                            className="navigation-button"
                                            onClick={(event) => handleAddToOrder(menuItem, event)}
                                        >
                                            Add to Order
                                        </button>
                                    </li>
                                </CSSTransition>
                            ))}
                        </TransitionGroup>
                    </div>
                </div>


            </div>
            <ToastContainer />
        </div>
    );
};

export default MenuComponent;
