import { createContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import io from 'socket.io-client';
import socket from './socket';

export const WebSocketContext = createContext({
    setNavigateFunction: () => { }
});

const WebSocketProvider = ({ children }) => {
    const navigate = useNavigate();

    useEffect(() => {
        socket.on('orderUpdatedCheckAndDisconnect', (order) => {
            console.log("orderUpdated event triggered", order);
            if (order.status === 'payed') {
                const storedUniqueIdentifier = localStorage.getItem('uniqueIdentifier');
                const storedTableId = localStorage.getItem('tableId');
                console.log("Stored uniqueIdentifier:", storedUniqueIdentifier);
                console.log("Stored tableId:", storedTableId);

                if (order.customer_identifier === storedUniqueIdentifier && order.table_number == storedTableId) {
                    console.log("Match found, deleting local storage items...");
                    localStorage.removeItem('uniqueIdentifier');
                    localStorage.removeItem('tableId');

                    const customerPaths = ['/', '/menu', '/order'];
                    if (customerPaths.includes(window.location.pathname)) {
                        console.log("Navigating to /scan...");
                        navigate('/scan');
                    }
                }
            }
        });

        return () => {
            socket.off('orderUpdatedCheckAndDisconnect');
        };
    }, [navigate]);


    return (
        <WebSocketContext.Provider value={{}}>
            {children}
        </WebSocketContext.Provider>
    );
};

export default WebSocketProvider;
