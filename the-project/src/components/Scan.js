import React from 'react';
import './Scan.css';

const Scan = () => {
  return (
    <div className="scan-container">
      <p className="scan-message">Please scan the QR code available on the table</p>
    </div>
  );
};

export default Scan;
