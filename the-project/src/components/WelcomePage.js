import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate, useParams } from 'react-router-dom';
import './WelcomePage.css';
import { useUniqueId } from './UniqueIdContext';
import { updateUniqueIdWithNickname, generateGuestUniqueId } from '../helpers/helpers';


const API_URL = process.env.REACT_APP_API_URL;

const WelcomePage = () => {
    const navigate = useNavigate();
    const { table_number } = useParams();
    const [nickname, setNickname] = useState('');

    const { uniqueIdentifier, setUniqueIdentifier } = useUniqueId();
    const [isLoading, setIsLoading] = useState(false);
    const [navigationStarted, setNavigationStarted] = useState(false);

    // check if table exists in the database
    useEffect(() => {
        const checkTableExistence = async () => {
            try {
                const response = await axios.get(`${API_URL}/tables/${table_number}`);

                // redirect to scan page if table doesn't exist
                if (response.data.error || response.data.table_number != table_number) {
                    navigate('/scan');
                }
            } catch (error) {
                console.error('Error checking table existence:', error);
                navigate('/scan');
            }
        };

        checkTableExistence();
    }, [table_number, navigate]);


    useEffect(() => {
        if (uniqueIdentifier.includes('@')) {
            navigate('/menu');
        }
        setTableId();
    }, [uniqueIdentifier, navigate]);


    const setTableId = () => {
        let tableId = localStorage.getItem('tableId');

        tableId = table_number;
        localStorage.setItem('tableId', tableId);
        return tableId;

    };


    const handleStartOrder = () => {
        let newIdentifier;
        if (nickname) {
            newIdentifier = updateUniqueIdWithNickname(nickname, uniqueIdentifier);
        } else {
            newIdentifier = generateGuestUniqueId(uniqueIdentifier);
        }
        setUniqueIdentifier(newIdentifier);

        if (newIdentifier.includes('@')) {
            navigate(`/menu`);
        }
    }

    return (
        <div className="welcome-container">
            <div className={`welcome-content ${isLoading ? 'hidden' : ''}`}>
                <h1 className="welcome-title">Welcome to Our Restaurant</h1>
                <p className="welcome-text">Get ready for a delightful dining experience!</p>
                <input
                    type="text"
                    className="nickname-input"
                    placeholder="Enter your nickname (Optional)"
                    value={nickname}
                    onChange={event => setNickname(event.target.value)}
                />
                <button className="start-order-button" onClick={handleStartOrder}>

                    {isLoading ? (
                        <div className="loading-container">
                            <div className="loading-circle"></div>
                        </div>
                    ) : (
                        'Start Ordering'
                    )}
                </button>
            </div>
            {isLoading && (
                <div className="loading-container">
                    <div className="loading-circle"></div>
                </div>
            )}
        </div>
    );
};

export default WelcomePage;
