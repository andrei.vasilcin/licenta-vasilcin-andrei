import React from 'react';
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend, CartesianGrid } from 'recharts';

const MostUsedIngredientsChart = ({ data }) => {
    // transform the data to display quantity in kilograms
    const dataInKg = data.map(item => ({
        name: item.name,
        'quantity (kg)': item.count / 1000,
    }));

    return (
        <BarChart
            width={1000}
            height={500}
            layout="vertical"
            data={dataInKg}
            margin={{
                top: 20, right: 30, left: 20, bottom: 5,
            }}
        >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis type="number" />
            <YAxis type="category" dataKey="name" />
            <Tooltip />
            <Legend />
            <Bar dataKey="quantity (kg)" fill="#8884d8" />
        </BarChart>
    );
};

export default MostUsedIngredientsChart;
