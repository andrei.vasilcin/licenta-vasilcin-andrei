const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/User')

const router = express.Router();

router.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;

        // find the user by username
        const user = await User.findOne({ where: { username } });

        // if user not found or password is wrong, send error
        if (!user || !bcrypt.compareSync(password, user.password)) {
            return res.status(401).json({ message: 'Invalid username or password' });
        }

        // if user is found and password is correct, create a token
        const token = jwt.sign({ userId: user.id }, '8zxcvbcncmc4', { expiresIn: '1h' });

        // send the token and user object back to the client
        res.json({ token, user });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});
router.get('/me', async (req, res) => {
    try {
        // get the token from the request headers
        const token = req.headers['Authorization'];

        if (!token) {
            return res.status(401).json({ message: 'No token provided' });
        }

        // verify the token
        let decoded;
        try {
            decoded = jwt.verify(token, '8zxcvbcncmc4');
        } catch (err) {
            return res.status(401).json({ message: 'Invalid token' });
        }

        // find the user by the ID stored in the token
        const user = await User.findOne({ where: { id: decoded.userId } });

        // if user not found, send an error
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // send the user object back to the client
        res.json({ user });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});


module.exports = router;
