const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const OrderController = require('../controllers/OrderController')(sequelize);
    // Define routes
    router.get('/', OrderController.getAllOrders);
    router.get('/:id', OrderController.getOrderById);
    router.post('/', OrderController.createOrder);
    router.put('/:id', OrderController.updateOrder);
    router.delete('/:id', OrderController.deleteOrder);
    router.post('/unconfirmed', OrderController.getUnconfirmedOrders);
    router.post('/confirm-unconfirmed/:customer_identifier/:tableNumber', OrderController.confirmUnconfirmedOrders);
    router.get('/confirmed/:tableNumber', OrderController.getConfirmedOrders);
    router.get('/all/confirmed', OrderController.getAllConfirmedOrders);

    router.post('/check-payment', OrderController.checkOrderPaymentStatus);
    router.post('/status', OrderController.checkOrderStatus);
    router.post('/status/:customer_identifier/:tableNumber', OrderController.checkOrderStatus);
    router.get('/stats/orders-over-time', OrderController.getOrdersOverTime);
    router.get('/stats/most-used-tables', OrderController.getMostUsedTables);
    router.get('/stats/most-used-ingredients', OrderController.getMostUsedIngredients);




    return router;
};
