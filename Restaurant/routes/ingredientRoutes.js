const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const IngredientController = require('../controllers/IngredientController')(sequelize);
    // Define routes
    router.get('/', IngredientController.getAllIngredients);
    router.get('/:id', IngredientController.getIngredientById);
    router.post('/', IngredientController.createIngredient);
    router.put('/:id', IngredientController.updateIngredient);
    router.delete('/:id', IngredientController.deleteIngredient);
    return router;
};