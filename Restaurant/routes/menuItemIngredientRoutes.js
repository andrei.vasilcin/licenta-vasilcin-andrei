const express = require('express');
const router = express.Router();


module.exports = sequelize => {
    const MenuItemIngredientController = require('../controllers/MenuItemIngredientController')(sequelize);
    // Define routes
    router.get('/', MenuItemIngredientController.getAllMenuItemIngredients);
    router.get('/:id', MenuItemIngredientController.getMenuItemIngredientById);
    router.post('/', MenuItemIngredientController.createMenuItemIngredient);
    router.put('/:id', MenuItemIngredientController.updateMenuItemIngredient);
    router.delete('/:id', MenuItemIngredientController.deleteMenuItemIngredient);

    return router;
};