const express = require('express');
const router = express.Router();



module.exports = sequelize => {
    const MenuItemController = require('../controllers/MenuItemController')(sequelize);
    // Define routes
    router.get('/', MenuItemController.getAllMenuItems);
    router.get('/:id', MenuItemController.getMenuItemById);
    router.post('/', MenuItemController.createMenuItem);
    router.put('/:id', MenuItemController.updateMenuItem);
    router.delete('/:id', MenuItemController.deleteMenuItem);
    router.get('/stats/order-statistics', MenuItemController.getMenuItemsOrderStatistics);

    return router;
};