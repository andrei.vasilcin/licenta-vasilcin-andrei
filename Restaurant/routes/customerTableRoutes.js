const express = require('express');
const router = express.Router();

module.exports = sequelize => {
    const CustomerTableController = require('../controllers/CustomerTableController')(sequelize);
    // Define routes
    router.get('/', CustomerTableController.getAllCustomerTables);
    router.get('/:id', CustomerTableController.getCustomerTableById);
    router.post('/', CustomerTableController.createCustomerTable);
    router.put('/:id', CustomerTableController.updateCustomerTable);
    router.delete('/:id', CustomerTableController.deleteCustomerTable);

    return router;
};