
const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Role = sequelize.define('Role', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
        },
    });

    return {
        // get all roles
        getAllRoles: async (req, res) => {
            try {
                const roles = await Role.findAll();
                res.json(roles);
            } catch (error) {
                console.error('Error fetching roles:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get role by ID
        getRoleById: async (req, res) => {
            const roleId = req.params.id;
            try {
                const role = await Role.findByPk(roleId);
                if (!role) {
                    res.status(404).json({ message: 'Role not found' });
                } else {
                    res.json(role);
                }
            } catch (error) {
                console.error('Error fetching role:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new role
        createRole: async (req, res) => {
            const { name, description } = req.body;
            try {
                const newRole = await Role.create({ name, description });
                res.status(201).json(newRole);
            } catch (error) {
                console.error('Error creating role:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update role by ID
        updateRole: async (req, res) => {
            const roleId = req.params.id;
            const { name, description } = req.body;
            try {
                const role = await Role.findByPk(roleId);
                if (!role) {
                    res.status(404).json({ message: 'Role not found' });
                } else {
                    await role.update({ name, description });
                    res.json(role);
                }
            } catch (error) {
                console.error('Error updating role:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete role by ID
        deleteRole: async (req, res) => {
            const roleId = req.params.id;
            try {
                const role = await Role.findByPk(roleId);
                if (!role) {
                    res.status(404).json({ message: 'Role not found' });
                } else {
                    await role.destroy();
                    res.json({ message: 'Role deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting role:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
