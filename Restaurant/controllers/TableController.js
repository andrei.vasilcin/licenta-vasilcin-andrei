

const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Table = sequelize.define('Table', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        table_number: {
            type: DataTypes.INTEGER,
        },
        capacity: {
            type: DataTypes.INTEGER,
        },
        status: {
            type: DataTypes.ENUM('available', 'reserved', 'occupied'),
            defaultValue: 'available',
        },
    });

    return {
        // get all tables
        getAllTables: async (req, res) => {
            try {
                const tables = await Table.findAll();
                res.json(tables);
            } catch (error) {
                console.error('Error fetching tables:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get table by ID
        getTableById: async (req, res) => {
            const tableId = req.params.id;
            try {
                const table = await Table.findByPk(tableId);
                if (!table) {
                    res.status(404).json({ message: 'Table not found' });
                } else {
                    res.json(table);
                }
            } catch (error) {
                console.error('Error fetching table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new table
        createTable: async (req, res) => {
            const { table_number, capacity, status } = req.body;
            try {
                const newTable = await Table.create({ table_number, capacity, status });
                res.status(201).json(newTable);
            } catch (error) {
                console.error('Error creating table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update table by ID
        updateTable: async (req, res) => {
            const tableId = req.params.id;
            const { table_number, capacity, status } = req.body;
            try {
                const table = await Table.findByPk(tableId);
                if (!table) {
                    res.status(404).json({ message: 'Table not found' });
                } else {
                    await table.update({ table_number, capacity, status });
                    res.json(table);
                }
            } catch (error) {
                console.error('Error updating table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete table by ID
        deleteTable: async (req, res) => {
            const tableId = req.params.id;
            try {
                const table = await Table.findByPk(tableId);
                if (!table) {
                    res.status(404).json({ message: 'Table not found' });
                } else {
                    await table.destroy();
                    res.json({ message: 'Table deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
