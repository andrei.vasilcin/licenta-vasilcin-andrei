const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Category = sequelize.define('Category', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            foreignKey: true,
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
        },
    });

    return {
        // get all categories
        getAllCategories: async (req, res) => {
            try {
                const categories = await Category.findAll();
                res.json(categories);
            } catch (error) {
                console.error('Error fetching categories:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get category by ID
        getCategoryById: async (req, res) => {
            const categoryId = req.params.id;
            try {
                const category = await Category.findByPk(categoryId);
                if (!category) {
                    res.status(404).json({ message: 'Category not found' });
                } else {
                    res.json(category);
                }
            } catch (error) {
                console.error('Error fetching category:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new category
        createCategory: async (req, res) => {

            const { name, description } = req.body;
            try {
                const newCategory = await Category.create({ name, description });
                res.status(201).json(newCategory);
            } catch (error) {
                console.error('Error creating category:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update category by ID
        updateCategory: async (req, res) => {
            const categoryId = req.params.id;
            const { name, description } = req.body;
            console.log("Desc:", description);
            console.log("name:", name);

            try {
                const category = await Category.findByPk(categoryId);
                if (!category) {
                    console.log("Category1:", category);

                    res.status(404).json({ message: 'Category not found' });
                } else {
                    const response = await category.update({ name, description });
                    console.log("response:", response);

                    res.json(category);
                }
            } catch (error) {
                console.error('Error updating category:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete category by ID
        deleteCategory: async (req, res) => {
            const categoryId = req.params.id;
            try {
                const category = await Category.findByPk(categoryId);
                if (!category) {
                    res.status(404).json({ message: 'Category not found' });
                } else {
                    await category.destroy();
                    res.json({ message: 'Category deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting category:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
