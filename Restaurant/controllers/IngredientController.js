
const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Ingredient = sequelize.define('Ingredient', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        unit: {
            type: DataTypes.STRING,
        },
        quantity: {
            type: DataTypes.DECIMAL(10, 2),
        },
        last_restock_date: {
            type: DataTypes.DATE,
        },
        expiry_date: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    });

    return {
        // get all ingredients
        getAllIngredients: async (req, res) => {
            try {
                const ingredients = await Ingredient.findAll();
                res.json(ingredients);
            } catch (error) {
                console.error('Error fetching ingredients:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get ingredient by ID
        getIngredientById: async (req, res) => {
            const ingredientId = req.params.id;
            try {
                const ingredient = await Ingredient.findByPk(ingredientId);
                if (!ingredient) {
                    res.status(404).json({ message: 'Ingredient not found' });
                } else {
                    res.json(ingredient);
                }
            } catch (error) {
                console.error('Error fetching ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new ingredient
        createIngredient: async (req, res) => {
            const { name, unit, quantity, last_restock_date, expiry_date } = req.body;
            try {
                const newIngredient = await Ingredient.create({
                    name,
                    unit,
                    quantity,
                    last_restock_date,
                    expiry_date,
                });
                res.status(201).json(newIngredient);
            } catch (error) {
                console.error('Error creating ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update ingredient by ID
        updateIngredient: async (req, res) => {
            const ingredientId = req.params.id;
            const { name, unit, quantity, last_restock_date, expiry_date } = req.body;
            try {
                const ingredient = await Ingredient.findByPk(ingredientId);
                if (!ingredient) {
                    res.status(404).json({ message: 'Ingredient not found' });
                } else {
                    await ingredient.update({
                        name,
                        unit,
                        quantity,
                        last_restock_date,
                        expiry_date,
                    });
                    res.json(ingredient);
                }
            } catch (error) {
                console.error('Error updating ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete ingredient by ID
        deleteIngredient: async (req, res) => {
            const ingredientId = req.params.id;
            try {
                const ingredient = await Ingredient.findByPk(ingredientId);
                if (!ingredient) {
                    res.status(404).json({ message: 'Ingredient not found' });
                } else {
                    await ingredient.destroy();
                    res.json({ message: 'Ingredient deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
