const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const MenuItemIngredient = sequelize.define('MenuItemIngredient', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quantity_required: {
            type: DataTypes.DECIMAL(10, 2),
        },
        menu_item_id: {
            type: DataTypes.INTEGER,
        },
        ingredient_id: {
            type: DataTypes.INTEGER,
        },
    });

    return {
        // get all menu item ingredients
        getAllMenuItemIngredients: async (req, res) => {
            try {
                const menuItemIngredients = await MenuItemIngredient.findAll();
                res.json(menuItemIngredients);
            } catch (error) {
                console.error('Error fetching menu item ingredients:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get menu item ingredient by ID
        getMenuItemIngredientById: async (req, res) => {
            const menuItemIngredientId = req.params.id;
            try {
                const menuItemIngredient = await MenuItemIngredient.findByPk(menuItemIngredientId);
                if (!menuItemIngredient) {
                    res.status(404).json({ message: 'Menu item ingredient not found' });
                } else {
                    res.json(menuItemIngredient);
                }
            } catch (error) {
                console.error('Error fetching menu item ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new menu item ingredient
        createMenuItemIngredient: async (req, res) => {
            const { menu_item_id, ingredient_id, quantity_required } = req.body;
            try {
                const newMenuItemIngredient = await MenuItemIngredient.create({ menu_item_id, ingredient_id, quantity_required });
                res.status(201).json(newMenuItemIngredient);
            } catch (error) {
                console.error('Error creating menu item ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update menu item ingredient by ID
        updateMenuItemIngredient: async (req, res) => {
            const menuItemIngredientId = req.params.id;
            const { menu_item_id, ingredient_id, quantity_required } = req.body;
            try {
                const menuItemIngredient = await MenuItemIngredient.findByPk(menuItemIngredientId);
                if (!menuItemIngredient) {
                    res.status(404).json({ message: 'Menu item ingredient not found' });
                } else {
                    await menuItemIngredient.update({ menu_item_id, ingredient_id, quantity_required });
                    res.json(menuItemIngredient);
                }
            } catch (error) {
                console.error('Error updating menu item ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete menu item ingredient by ID
        deleteMenuItemIngredient: async (req, res) => {
            const menuItemIngredientId = req.params.id;
            try {
                const menuItemIngredient = await MenuItemIngredient.findByPk(menuItemIngredientId);
                if (!menuItemIngredient) {
                    res.status(404).json({ message: 'Menu item ingredient not found' });
                } else {
                    await menuItemIngredient.destroy();
                    res.json({ message: 'Menu item ingredient deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting menu item ingredient:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
