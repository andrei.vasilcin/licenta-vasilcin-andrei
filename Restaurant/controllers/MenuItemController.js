const { DataTypes } = require('sequelize');
const Order = require('../models/Order');
module.exports = sequelize => {
    const MenuItem = sequelize.define('MenuItem', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
        },
        price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
        },
        image_url: {
            type: DataTypes.STRING,
        },
        category_id: {
            type: DataTypes.INTEGER,
        },
        availability: {
            type: DataTypes.ENUM('available', 'unavailable'),
            defaultValue: 'available',
        },
    });
    Order.belongsTo(MenuItem, { foreignKey: 'menu_item_id' });
    MenuItem.hasMany(Order, { foreignKey: 'menu_item_id' });
    return {
        // get all menu items
        getAllMenuItems: async (req, res) => {
            try {
                const menuItems = await MenuItem.findAll();
                res.json(menuItems);
            } catch (error) {
                console.error('Error fetching menu items:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // get menu item by ID
        getMenuItemById: async (req, res) => {
            const menuItemId = req.params.id;
            try {
                const menuItem = await MenuItem.findByPk(menuItemId);
                if (!menuItem) {
                    res.status(404).json({ message: 'Menu item not found' });
                } else {
                    res.json(menuItem);
                }
            } catch (error) {
                console.error('Error fetching menu item:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new menu item
        createMenuItem: async (req, res) => {
            const { name, description, price, category_id, availability } = req.body;
            const image_url = '/assets/images/' + req.file.filename; // get the uploaded file's name
            try {
                const newMenuItem = await MenuItem.create({ name, description, price, image_url, category_id, availability });
                res.status(201).json(newMenuItem);
            } catch (error) {
                console.error('Error creating menu item:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update menu item by ID
        // update menu item by ID
        updateMenuItem: async (req, res) => {
            const menuItemId = req.params.id;
            const { name, description, price, category_id, availability, existingImageUrl } = req.body;
            let image_url;

            try {
                const menuItem = await MenuItem.findByPk(menuItemId);
                if (!menuItem) {
                    res.status(404).json({ message: 'Menu item not found' });
                    return;
                }

                // check if a new image file is provided
                if (req.file) {
                    image_url = '/assets/images/' + req.file.filename; // get the uploaded file's name
                } else if (existingImageUrl) {
                    // use the existing image URL if no new image is uploaded
                    image_url = existingImageUrl;
                } else {
                    // retain the current image_url if neither a new image nor an existingImageUrl is provided
                    image_url = menuItem.image_url;
                }

                await menuItem.update({ name, description, price, image_url, category_id, availability });
                res.json(menuItem);
            } catch (error) {
                console.error('Error updating menu item:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },


        // delete menu item by ID
        deleteMenuItem: async (req, res) => {
            const menuItemId = req.params.id;
            try {
                const menuItem = await MenuItem.findByPk(menuItemId);
                if (!menuItem) {
                    res.status(404).json({ message: 'Menu item not found' });
                } else {
                    await menuItem.destroy();
                    res.json({ message: 'Menu item deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting menu item:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        getMenuItemsOrderStatistics: async (req, res) => {
            try {
                const menuItems = await MenuItem.findAll({
                    attributes: [
                        'name',
                        [sequelize.fn('COUNT', sequelize.col('Orders.id')), 'orders']
                    ],
                    include: {
                        model: Order,
                        attributes: []
                    },
                    group: ['MenuItem.id'],
                    order: [[sequelize.fn('COUNT', sequelize.col('Orders.id')), 'DESC']]
                });

                res.json(menuItems);
            } catch (error) {
                console.error('Error fetching menu items orders statistics:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
