const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const CustomerTable = sequelize.define('CustomerTable', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        table_number: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        capacity: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        is_available: {
            type: DataTypes.BOOLEAN,
            defaultValue: true,
        },
    });

    return {
        // get all customer tables
        getAllCustomerTables: async (req, res) => {
            try {
                const customerTables = await CustomerTable.findAll();
                res.json(customerTables);
            } catch (error) {
                console.error('Error fetching customer tables:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get customer table by ID
        getCustomerTableById: async (req, res) => {
            const customerTableId = req.params.id;
            try {
                const customerTable = await CustomerTable.findByPk(customerTableId);
                if (!customerTable) {
                    res.status(404).json({ message: 'Customer table not found' });
                } else {
                    res.json(customerTable);
                }
            } catch (error) {
                console.error('Error fetching customer table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new customer table
        createCustomerTable: async (req, res) => {
            const { customer_identifier, table_number } = req.body;
            try {
                const newCustomerTable = await CustomerTable.create({ customer_identifier, table_number });
                res.status(201).json(newCustomerTable);
            } catch (error) {
                console.error('Error creating customer table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update customer table by ID
        updateCustomerTable: async (req, res) => {
            const customerTableId = req.params.id;
            const { customer_identifier, table_number } = req.body;
            try {
                const customerTable = await CustomerTable.findByPk(customerTableId);
                if (!customerTable) {
                    res.status(404).json({ message: 'Customer table not found' });
                } else {
                    await customerTable.update({ customer_identifier, table_number });
                    res.json(customerTable);
                }
            } catch (error) {
                console.error('Error updating customer table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete customer table by ID
        deleteCustomerTable: async (req, res) => {
            const customerTableId = req.params.id;
            try {
                const customerTable = await CustomerTable.findByPk(customerTableId);
                if (!customerTable) {
                    res.status(404).json({ message: 'Customer table not found' });
                } else {
                    await customerTable.destroy();
                    res.json({ message: 'Customer table deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting customer table:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
