
const { DataTypes } = require('sequelize');

module.exports = sequelize => {
    const Transaction = sequelize.define('Transaction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        type: {
            type: DataTypes.ENUM('payment', 'refund', 'expense'),
        },
        amount: {
            type: DataTypes.DECIMAL(10, 2),
        },
        timestamp: {
            type: DataTypes.DATE,
        },
        notes: {
            type: DataTypes.TEXT,
        },
        customer_identifier: {
            type: DataTypes.INTEGER,
        },
    });

    return {
        // get all transactions
        getAllTransactions: async (req, res) => {
            try {
                const transactions = await Transaction.findAll();
                res.json(transactions);
            } catch (error) {
                console.error('Error fetching transactions:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get transaction by ID
        getTransactionById: async (req, res) => {
            const transactionId = req.params.id;
            try {
                const transaction = await Transaction.findByPk(transactionId);
                if (!transaction) {
                    res.status(404).json({ message: 'Transaction not found' });
                } else {
                    res.json(transaction);
                }
            } catch (error) {
                console.error('Error fetching transaction:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new transaction
        createTransaction: async (req, res) => {
            const { type, amount, timestamp, notes, user_id } = req.body;
            try {
                const newTransaction = await Transaction.create({
                    type,
                    amount,
                    timestamp,
                    notes,
                    user_id,
                });
                res.status(201).json(newTransaction);
            } catch (error) {
                console.error('Error creating transaction:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update transaction by ID
        updateTransaction: async (req, res) => {
            const transactionId = req.params.id;
            const { type, amount, timestamp, notes, user_id } = req.body;
            try {
                const transaction = await Transaction.findByPk(transactionId);
                if (!transaction) {
                    res.status(404).json({ message: 'Transaction not found' });
                } else {
                    await transaction.update({
                        type,
                        amount,
                        timestamp,
                        notes,
                        user_id,
                    });
                    res.json(transaction);
                }
            } catch (error) {
                console.error('Error updating transaction:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete transaction by ID
        deleteTransaction: async (req, res) => {
            const transactionId = req.params.id;
            try {
                const transaction = await Transaction.findByPk(transactionId);
                if (!transaction) {
                    res.status(404).json({ message: 'Transaction not found' });
                } else {
                    await transaction.destroy();
                    res.json({ message: 'Transaction deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting transaction:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
    };
};
