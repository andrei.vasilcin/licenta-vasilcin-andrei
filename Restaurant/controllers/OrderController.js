const { DataTypes, or } = require('sequelize');
const Table = require('../models/Table');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const TemporaryOrder = require('../models/TemporaryOrder');
const MenuItem = require('../models/MenuItem');
const Ingredient = require('../models/Ingredient');
const MenuItemIngredient = require('../models/MenuItemIngredient');

const { io } = require('../app');

module.exports = sequelize => {
    const Order = sequelize.define('Order', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        table_number: {
            type: DataTypes.INTEGER,
        },
        customer_identifier: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        total_price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
        },
        status: {
            type: DataTypes.ENUM('pending', 'in progress', 'completed', 'payed', 'confirmed', 'ready to serve'),
            defaultValue: 'pending',
        },
        order_time: {
            type: DataTypes.DATE,
        },
        quantity: {
            type: DataTypes.INTEGER,
        },
        remark: {
            type: DataTypes.TEXT,
        },
    });
    Order.belongsTo(MenuItem, { foreignKey: 'menu_item_id', as: 'menuItem' });
    Order.belongsTo(Table, { foreignKey: 'table_number' });
    Order.hasMany(TemporaryOrder, { foreignKey: 'customer_identifier' });

    return {

        // get all orders
        getAllOrders: async (req, res) => {
            try {
                const orders = await Order.findAll();
                res.json(orders);
            } catch (error) {
                console.error('Error fetching orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get order by ID
        getOrderById: async (req, res) => {
            const orderId = req.params.id;
            try {
                const order = await Order.findByPk(orderId);
                if (!order) {
                    res.status(404).json({ message: 'Order not found' });
                } else {
                    res.json(order);
                }
            } catch (error) {
                console.error('Error fetching order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // create a new order
        createOrder: async (req, res) => {
            const { table_number, customer_identifier, total_price, status, order_time, menu_item_id, quantity, remark } = req.body;
            try {
                const newOrder = await Order.create({
                    table_number,
                    customer_identifier,
                    total_price,
                    status,
                    order_time,
                    menu_item_id,
                    quantity,
                    remark,
                });
                // emit a real-time event to notify clients about the new order
                res.status(201).json(newOrder);
            } catch (error) {
                console.error('Error creating order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // update order by ID
        updateOrder: async (req, res) => {
            const orderId = req.params.id;
            const { table_number, customer_identifier, total_price, status, order_time, menu_item_id, quantity, remark } = req.body;
            try {
                const order = await Order.findByPk(orderId);
                if (!order) {
                    res.status(404).json({ message: 'Order not found' });
                } else {
                    await order.update({
                        table_number,
                        customer_identifier,
                        total_price,
                        status,
                        order_time,
                        menu_item_id,
                        quantity,
                        remark,
                    });
                    // emit a real-time event to notify clients about the updated order
                    console.log("Emiting updated order:", order);
                    req.io.emit('orderUpdated', order);
                    if (order.status === 'payed') {
                        console.log("Emiting orderUpdatedCheckAndDisconnect:", order);
                        req.io.emit('orderUpdatedCheckAndDisconnect', order);
                    }

                    res.json(order);
                }
            } catch (error) {
                console.error('Error updating order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // delete order by ID
        deleteOrder: async (req, res) => {
            const orderId = req.params.id;
            try {
                const order = await Order.findByPk(orderId);
                if (!order) {
                    res.status(404).json({ message: 'Order not found' });
                } else {
                    await order.destroy();
                    // emit a real-time event to notify clients about the deleted order
                    req.io.emit('orderDeleted', order);
                    res.json({ message: 'Order deleted successfully' });
                }
            } catch (error) {
                console.error('Error deleting order:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        // get all unconfirmed orders (temporary orders)
        getUnconfirmedOrders: async (req, res) => {
            const { uniqueIdentifier, tableNumber } = req.body;

            try {
                const unconfirmedOrders = await Order.findAll({
                    where: {
                        customer_identifier: uniqueIdentifier,
                        table_number: tableNumber,
                        status: 'pending',
                    },
                });
                res.json(unconfirmedOrders);
            } catch (error) {
                console.error('Error fetching unconfirmed orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // confirm unconfirmed orders and move them to the confirmed orders
        confirmUnconfirmedOrders: async (req, res) => {
            const tableNumber = req.params.tableNumber;
            const uniqueIdentifier = req.params.customer_identifier;

            try {
                const unconfirmedOrders = await TemporaryOrder.findAll({

                    where: {
                        customer_identifier: uniqueIdentifier,
                        table_number: tableNumber,
                        status: 'temporary',
                    },
                });
                let allNewOrders = [];
                // create new confirmed orders based on unconfirmed orders
                for (const unconfirmedOrder of unconfirmedOrders) {
                    const newOrder = await Order.create({
                        table_number: unconfirmedOrder.table_number,
                        customer_identifier: unconfirmedOrder.customer_identifier,
                        total_price: unconfirmedOrder.total_price,
                        status: 'pending',
                        order_time: unconfirmedOrder.order_time,
                        menu_item_id: unconfirmedOrder.menu_item_id,
                        quantity: unconfirmedOrder.quantity,
                        remark: unconfirmedOrder.remark,
                    });

                    allNewOrders.push(newOrder);
                    // delete the unconfirmed order
                    await unconfirmedOrder.destroy();
                }
                console.log("starting emitting for: ", allNewOrders);
                req.io.emit('ordersCreated', allNewOrders);
                res.json({ message: 'Unconfirmed orders confirmed successfully' });
            } catch (error) {
                console.error('Error confirming unconfirmed orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // fetch confirmed orders by unique identifier
        getConfirmedOrders: async (req, res) => {
            const tableNumber = req.params.tableNumber;


            try {
                const confirmedOrders = await Order.findAll({
                    include: {
                        model: MenuItem,
                        as: 'menuItem',
                        attributes: ['image_url', 'name']
                    },
                    where: {
                        table_number: tableNumber,
                        [Op.or]: [
                            { status: 'pending' },
                            { status: 'confirmed' },
                            { status: 'in progress' },
                            { status: 'ready to serve' },

                        ],


                    },
                });
                res.json(confirmedOrders);
            } catch (error) {
                console.error('Error fetching confirmed orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        getAllConfirmedOrders: async (req, res) => {



            try {
                const confirmedOrders = await Order.findAll({
                    include: {
                        model: MenuItem,
                        as: 'menuItem',
                        attributes: ['id', 'image_url', 'name']
                    },
                    where: {
                        [Op.or]: [
                            { status: 'in progress' },
                            { status: 'confirmed' }
                        ],

                    },
                });
                res.json(confirmedOrders);
            } catch (error) {
                console.error('Error fetching confirmed orders:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // check if an order has been paid
        checkOrderPaymentStatus: async (req, res) => {
            const { uniqueIdentifier, tableNumber } = req.body;

            try {
                const order = await Order.findOne({
                    where: {
                        customer_identifier: uniqueIdentifier,
                        table_number: tableNumber,
                    },
                });

                if (order && order.status === 'payed') {
                    res.json({ status: 'payed' });
                } else {
                    res.json({ status: 'not payed' });
                }
            } catch (error) {
                console.error('Error checking order payment status:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },

        checkOrderStatus: async (req, res) => {
            const tableNumber = req.params.tableNumber;
            const uniqueIdentifier = req.params.customer_identifier;
            try {
                const order = await Order.findOne({
                    where: {
                        customer_identifier: uniqueIdentifier,
                        table_number: tableNumber,

                    },
                    order: [
                        ['id', 'DESC'],
                    ],
                });

                if (order) {
                    res.json({ status: order.status });
                } else {
                    res.json({ status: 'not_found' });
                }
            } catch (error) {
                console.error('Error checking order status:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // get orders over time
        getOrdersOverTime: async (req, res) => {
            try {
                const ordersOverTime = await Order.findAll({
                    attributes: [
                        [sequelize.fn('DATE', sequelize.col('order_time')), 'date'],
                        [sequelize.fn('COUNT', sequelize.col('id')), 'count']
                    ],
                    group: [sequelize.fn('DATE', sequelize.col('order_time'))],
                    order: [[sequelize.fn('DATE', sequelize.col('order_time')), 'ASC']]
                });
                res.json(ordersOverTime);
            } catch (error) {
                console.error('Error fetching orders over time:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        // get most used tables
        getMostUsedTables: async (req, res) => {
            try {
                const mostUsedTables = await Order.findAll({
                    attributes: ['table_number', [sequelize.fn('COUNT', sequelize.col('table_number')), 'count']],
                    group: ['table_number'],
                    order: [[sequelize.literal('count'), 'DESC']]
                });

                const formattedResult = mostUsedTables.map(table => ({
                    tableNumber: table.table_number,
                    count: table.getDataValue('count')
                }));

                res.json(formattedResult);
            } catch (error) {
                console.error('Error fetching most used tables:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },
        getMostUsedIngredients: async (req, res) => {
            try {
                // get all orders with related menu items and ingredients
                const orders = await Order.findAll({
                    include: {
                        model: MenuItem,
                        as: 'menuItem',
                        include: {
                            model: MenuItemIngredient,
                            as: 'ingredients',
                            include: {
                                model: Ingredient,
                                as: 'Ingredient',
                            }
                        }
                    }
                });

                // calculate the usage count for each ingredient
                const ingredientUsage = {};
                orders.forEach(order => {
                    order.menuItem.ingredients.forEach(menuItemIngredient => {
                        // access the Ingredient object using the correct alias
                        const ingredient = menuItemIngredient.Ingredient;
                        const ingredientId = ingredient.id;
                        if (!ingredientId) {
                            console.log('Skipping an ingredient without an ID');
                            return;
                        }
                        const ingredientName = ingredient.name;
                        const usageCount = menuItemIngredient.quantity_required * order.quantity;

                        if (ingredientUsage[ingredientId]) {
                            ingredientUsage[ingredientId].usageCount += usageCount;
                        } else {
                            ingredientUsage[ingredientId] = { ingredientName, usageCount };
                        }
                    });
                });

                // convert to an array and sort by usage count
                const mostUsedIngredients = Object.values(ingredientUsage).sort((a, b) => b.usageCount - a.usageCount);

                res.json(mostUsedIngredients);
            } catch (error) {
                console.error('Error fetching most used ingredients:', error);
                res.status(500).json({ message: 'Internal server error' });
            }
        },




    };
};
