// models/Ingredient.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const MenuItemIngredient = require('./MenuItemIngredient');
const Ingredient = sequelize.define('Ingredient', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    unit: {
        type: DataTypes.STRING,
    },
    quantity: {
        type: DataTypes.DECIMAL(10, 2),
    },
    last_restock_date: {
        type: DataTypes.DATE,
    },
    expiry_date: {
        type: DataTypes.DATE,
        allowNull: false,
    },
});
// Ingredient.hasMany(MenuItemIngredient, { foreignKey: 'ingredient_id' });
// Ingredient.hasMany(MenuItemIngredient, { foreignKey: 'ingredient_id' });
module.exports = Ingredient;
