// models/MenuItem.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const MenuItemIngredient = require('./MenuItemIngredient');
const MenuItem = sequelize.define('MenuItem', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.TEXT,
    },
    price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
    },
    image_url: {
        type: DataTypes.STRING,
    },
    category_id: {
        type: DataTypes.INTEGER,
    },
    availability: {
        type: DataTypes.ENUM('available', 'unavailable'),
        defaultValue: 'available',
    },
});


module.exports = MenuItem;
