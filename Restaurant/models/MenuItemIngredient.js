// models/MenuItemIngredient.js
const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const MenuItem = require('./MenuItem');
const Ingredient = require('./Ingredient');
const MenuItemIngredient = sequelize.define('MenuItemIngredient', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    quantity_required: {
        type: DataTypes.DECIMAL(10, 2),
    },
    menu_item_id: {
        type: DataTypes.INTEGER,
    },
    ingredient_id: {
        type: DataTypes.INTEGER,
    },
});

module.exports = MenuItemIngredient;
